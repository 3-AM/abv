$(function() {
  'use strict';

  (function(){

    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
      return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
      };
    });

    $('.set-filters').on('click', '.itemset-item', function() {
      var text = $(this).find('span').text();
      debugger;
      if (text === 'Все товары') {
        $('.combo, .product').show();
      } else {
        $('.combo').hide();
        $('.product').hide();
        $('.product:contains(" '+text+'")').show();
      }
    });

    // +all
    var $catalog = $('.content .catalog');
    var catalog = ko.dataFor($catalog.get(0));

    $('.set-purchase-all').on('click', function() {
      var products = catalog.products();
      for (var i=0; i<products.length; i++) {
        products[i].productPlus();
      }
    });

    ko.applyBindings(catalog, $('.sidebar-set-price').get(0));

  })();

});
