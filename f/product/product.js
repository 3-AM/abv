;(function($, window, document, undefined) {
  'use strict';

  $(function() {
    var product = new Product(window.abv.product);

    product.toggleDetails = function(product, event) {
      $('.product-details').stop(true).slideToggle('fast');
    };
    product.productPlus = function() {
      window.abv.basket.addProduct(product);
    };
    product.productMinus = function() {
      window.abv.basket.removeProduct(product);
    };

    product.onRenderCustom = function(elements, data) {
      product.onRender(elements, data);
      product.$self.find('.switcher').switcher().on('switch.switcher', function (event, item) {
        var target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        var product = ko.dataFor(target);
        product.updateSwitcher(item);
      });
    };

    ko.applyBindings(product, $('.product-card').get(0));
  });

})(jQuery, window, document);