;
(function ($, window, document, undefined) {
  'use strict';

  $(function () {
    var placemarks = [];
    // init map
    ymaps.ready(function () {

      // placemark
      placemarks.push(new ymaps.Placemark([55.8, 37.6], {
        hintContent: 'Анненский',
        balloonContentHeader: '<a href="/point/">Анненский</a>',
        balloonContentBody:
          '<b>Адрес:</b> Анненский пр., д. 1, стр. 20, Москва<br/>'+
            '<b>Телефон:</b> +7 499 394-19-51<br/>'+
            '<b>Электронная почта:</b> <a href="info@abv-store.ru">info@abv-store.ru</a><br/>'
      }, {
        iconImageHref: '/f/placemark.png',
        iconImageSize: [46, 47],
        iconImageOffset: [-15, -43]
      }));
      placemarks.push(new ymaps.Placemark([55.83, 37.5], {
        hintContent: 'Марьинский',
        balloonContentHeader: '<a href="/point/">Марьинский</a>',
        balloonContentBody:
          '<b>Адрес:</b> Анненский пр., д. 1, стр. 20, Москва<br/>'+
            '<b>Телефон:</b> +7 499 394-19-51<br/>'+
            '<b>Электронная почта:</b> <a href="info@abv-store.ru">info@abv-store.ru</a><br/>'
      }, {
        iconImageHref: '/f/placemark.png',
        iconImageSize: [46, 47],
        iconImageOffset: [-15, -43]
      }));
      placemarks.push(new ymaps.Placemark([55.66, 37.6], {
        hintContent: 'Константинопольский',
        balloonContentHeader: '<a href="/point/">Константинопольский</a>',
        balloonContentBody:
          '<b>Адрес:</b> Анненский пр., д. 1, стр. 20, Москва<br/>'+
            '<b>Телефон:</b> +7 499 394-19-51<br/>'+
            '<b>Электронная почта:</b> <a href="info@abv-store.ru">info@abv-store.ru</a><br/>'
      }, {
        iconImageHref: '/f/placemark.png',
        iconImageSize: [46, 47],
        iconImageOffset: [-15, -43]
      }));
      var map = new ymaps.Map('map', {
        center: placemarks[0].geometry.getCoordinates(),
        zoom: 10
      });
      map.controls.add('mapTools');
      map.controls.add('zoomControl');
      map.controls.add('scaleLine');
      for (var i=0; i<placemarks.length; i++) {
        map.geoObjects.add(placemarks[i]);
      }
      window.abv.map = map;

      // auto locator
      $('.pickup-point-locator').on('click', function(event) {
        window.abv.map.setCenter(placemarks[$(this).data('placemark')].geometry.getCoordinates(), 15);
        $('body, html').animate({scrollTop: $('#map').offset().top-10});
        event.preventDefault();
      });
    });
  });

})(jQuery, window, document);