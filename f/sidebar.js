;(function($, window, document, undefined) {
  'use strict';

  $(function() {

    $('.ribbon').affix();
	if($('.top:visible').length) {
		$('.ribbon').addClass('has_top');
	}

    $('.sidebar-login a').on('click', function(event) {
      $('.header-search').toggleClass('header-search-fade');
      $('.sidebar-login-window').toggle();
      event.preventDefault();
      event.stopPropagation();
    });

    $('.sidebar-login-window').on('click', function(event) {
      event.stopPropagation();
    });

    $('.sidebar-login-window button').on('click', function(event) {
      $(this).parents('form').find('input[type="submit"]').click();
	  return false;
    });

    $('.sidebar-login-close').on('click', function(event) {
      $('.sidebar-login-window').hide();
      $('.header-search').removeClass('header-search-fade');
    });

    $('.sidebar-login-controls .link-pseudo').on('click', function() {
      $('.sidebar-login-window').toggleClass('sidebar-login-recover');
    });

    $('.header-search input').on('focus', function(event) {
      $('.sidebar-login-window').hide();
      $('.header-search').removeClass('header-search-fade');
    });

  });

})(jQuery, window, document);