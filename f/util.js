/*
 * --
 * solodoff@ya.ru
 * upd 19.11.2013
 * --
 *
 */

'use strict';

var util = util || {};

util.formatPrice = function(amount) {
  var splitted = (amount.toFixed(2)+'').split('.');
  var hi = splitted[0];
  var lo = splitted[1];
  if (hi.length > 4) {
    // don't even ask :)
    hi = hi.split('').reverse().join('').match(/.{1,3}/g).join(' ').split('').reverse().join('');
  }

  return  '<span class="price">'+
    '<span class="price-amount">'+
    '<span class="price-amount-hi">'+hi+'</span>'+
    '<span class="price-amount-lo">'+lo+'</span>'+
    '</span>'+
    ' '+
    '<span class="price-currency rub">руб.</span>'+
    '</span>';
};


util.decl = function(number, titles) {
  var cases = [2, 0, 1, 1, 1, 2];
  return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
};

util.decl_roubles = function(number) {
  return decl(number, ['рубль', 'рубля', 'рублей']);
};

/**
 * Заменяет спецсимволы HTML мнемониками для корректного отображения в браузере
 * @param {string} text текст
 * @return {string} текст с мнемониками
 */
util.escape_html = function(text) {
  return $('<div />').text(text).html();
};

/**
 * Форматирование чисел
 * Пример с пробелом 1234567.1234 > 1 234 567.1234
 *
 * @param {Number} num
 * @param {String|Number} delimiter строка или код символа
 * @return {String}
 */
util.format_num = function(num, delimiter) {
  delimiter = delimiter || 8201; //&thinsp

  if (typeof(delimiter) == 'number') {
    delimiter = String.fromCharCode(delimiter);
  }
  var parts = num.toString().split('.');
  return parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1' + delimiter) + (parts[1] ? '.' + parts[1] : '');
};

util.getURLParameter = function(name) {
  return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
};

util.getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

util.roundFloat = function(number, precision) {
  var x = Math.pow(10, precision);
  return Math.round(number * x) / x;
};

/**
 * Поиск в массиве объектов
 * @param {Array} arr
 * @param {String} prop имя свойства объекта, по котому идет поиск
 * @param {} s искомое значение
 * @return {Object|null}
 */
util.searchObj = function(arr, prop, s) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i][prop] == s) {
      return arr[i];
    }
  }
  return null;
};

/**
 * Перемешивает массив
 * @param {Array} array
 * @return {Array}
 */
util.arrayShuffle = function(array) {
  for (var i = array.length - 1; i > 0; i--) {
    var num = Math.floor(Math.random() * (i + 1));
    var d = array[num];
    array[num] = array[i];
    array[i] = d;
  }
  return array;
};

util.proportion = function(a1, a2, a, b1, b2) {
  var p = (a - a1) / (a2 - a1);
  if (p < 0) {
    p = 0;
  }
  if (p > 1) {
    p = 1;
  }
  return b1 + (b2 - b1) * p;
};

/**
 *
 * @param uri
 */
util.redirect = function(uri) {
  window.location = uri;
};

/**
 * Плавно скроллит страницу до указанного элемента
 * @param $elem
 * @param options
 */
util.scrollTo = function($elem, options) {
  var $window = $(window);

  var animOptions = $.extend({}, options, {
    'step': function(now) {
      window.scrollTo(0, Math.round(now));
    }
  });

  $('<div/>')
    .css('top', $window.scrollTop() + 'px')
    .animate({
      'top': $elem.offset().top + 'px'
    }, animOptions);
};