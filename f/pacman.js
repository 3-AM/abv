// Pacman
function Pacman(duration) {
  var self = this;
  duration = duration || 0;
  this.counter = ko.observable(duration);
  this.onRender = function (elements, data) {
    self.$self = $(elements[0]).parent(); // hack :)
    var $path = self.$self.find('.pacman-path');
    // init path
    $path.append($('<span class="pacman-himself"/>'));
    for (var i=1; i<duration; i++) {
      var food = ['pacman-icecream', 'pacman-cherry', 'pacman-apple'];
      var cls = (i%5===0 ? food[Math.ceil(Math.random()*food.length)-1] : 'pacman-dot');
      $path.append($('<span class="'+cls+'"/>'));
    }
    $path.append($('<span class="pacman-monster"/>'));
    $path.width($path.width());
    /*var interval = window.setInterval(function() {
      $path.find('span:eq(1)').remove();
      var counter = self.counter()-1;
      self.counter(counter);
      if (counter < 0) {
        clearInterval(interval);
      }
    }, 10000);*/
  };
  this.getTimeLeft = function() {
    var counter = this.counter();
    if (counter > 0) {
      return 'До конца акции осталось '+counter+' '+util.decl(counter, ['день', 'дня', 'дней']);
    } else {
      return 'Акция завершена';
    }
  }
}

(function ($, window, document, undefined) {
  'use strict';

  $(function () {
    $('.pacman').each(function () {
      var duration = $(this).data('duration');
      ko.applyBindings(new Pacman(duration), this);
    });
  });

})(jQuery, window, document);