;(function ($, window, document, undefined) {
  'use strict';

  $(function () {
    // init map
    ymaps.ready(function () {

      // placemark
      var placemark = new ymaps.Placemark([55.8, 37.6], {
        hintContent: 'Анненский',
        balloonContentHeader: '<a href="/point/">Анненский</a>',
        balloonContentBody:
          '<b>Адрес:</b> Анненский пр., д. 1, стр. 20, Москва<br/>'+
            '<b>Телефон:</b> +7 499 394-19-51<br/>'+
            '<b>Электронная почта:</b> <a href="info@abv-store.ru">info@abv-store.ru</a><br/>'
      }, {
        iconImageHref: '/f/placemark.png',
        iconImageSize: [46, 47],
        iconImageOffset: [-15, -43]
      });
      var map = new ymaps.Map('map', {
        center: placemark.geometry.getCoordinates(),
        zoom: 10
      });
      map.controls.add('mapTools');
      map.controls.add('zoomControl');
      map.controls.add('scaleLine');
      map.geoObjects.add(placemark);
      window.abv.map = map;

      // auto locator
      $('.pickup-point-locator').on('click', function(event) {
        window.abv.map.setCenter(placemark.geometry.getCoordinates(), 15);
        $('body, html').animate({scrollTop: $('#map').offset().top-10});
        event.preventDefault();
      });
    });
  });

})(jQuery, window, document);