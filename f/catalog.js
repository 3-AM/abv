// Product
function Product(data) {
  for (var key in data) {
    if (data.hasOwnProperty(key)) {
      this[key] = data[key];
    }
  }
  if (this.variants) {
    if (!$.isArray(this.variants.variant)) {
      this.variants.variant = [this.variants.variant];
    }
  } else {
    this.variants = {
      variant: [
        {
          price: this.price ? this.price : 0
        }
      ]
    };
  }
  this.rating = Math.random();
  this.newness = Math.random();
  this.price = ko.observable(parseFloat(this.variants.variant[0].price));
  this.oldprice = ko.observable(parseFloat(this.variants.variant[0].oldprice));
  this.amount = ko.observable(this.variants.variant[0].amount);
  this.counter = ko.observable(1);
  this.ready = ko.observable(false);

  this.onImageError = function (product, event) {
    var target = (event.currentTarget) ? event.currentTarget : event.srcElement;
    target.src = product.image = '/f/products/none.jpg';
  };
  this.onImageLoad = function (product, event) {
    this.ready(true);
  };
  this.onRender = function (elements, data) {
    data.$self = $(elements[0]).parent(); // hack :)
    if (data.getCount() > 0) {
      data.$self.find('.product-counter').removeClass('product-counter-hidden');
      data.$self.find('.product-count-minus').removeClass('product-count-minus-hidden');
    }
  };
  this.purchasedPrice = ko.observable(0);
  this.getPrice = function () {
    return this.price();
  };
  this.getOldPrice = function() {
    return this.oldprice();
  };
  this.hasOldPrice = function() {
    return !isNaN(this.getOldPrice());

  };
  this.getVariants = function () {
    return '[' + $.map(this.variants.variant,function (key) {
      return '"' + key.amount + '"';
    }).join(',') + ']';
  };
  this.hasVariants = function () {
    return this.variants.variant.length > 1;
  };
  this.updateSwitcher = function (item) {
    for (var i = 0; i < this.variants.variant.length; i++) {
      if (this.variants.variant[i].amount === item) {
        this.price(this.variants.variant[i].price);
        this.oldprice(this.variants.variant[i].oldprice);
        this.amount(this.variants.variant[i].amount);
        return;
      }
    }
  };
  this.productMinus = function () {
    if (this.getCount() <= 0) {
      return;
    }
    window.abv.basket.removeProduct(this);
  };
  this.productPlus = function () {
    if (this.getCount() >= 99) {
      return;
    }
    window.abv.basket.addProduct(this);
  };
  this.getCount = function () {
    this.lastCount = this.lastCount || 0;
    var count = window.abv.basket.getTotalCount(this.name);

    if (this.lastCount != count) {
      var $counter = this.$self.find('.product-counter');
      var $minus = this.$self.find('.product-count-minus');
      if (isNaN(count) || (count == 0)) {
        $counter.removeClass('product-counter-pulse').addClass('product-counter-hidden');
        $minus.addClass('product-count-minus-hidden');
      } else {
        var pulse_class = (count > this.lastCount ? 'product-counter-pulse-plus' : 'product-counter-pulse-minus');
        $counter.removeClass('product-counter-hidden').addClass(pulse_class);
        setTimeout(function () {
          $counter.removeClass(pulse_class);
          $minus.removeClass('product-count-minus-hidden');
        }, 100);
      }
    }
    this.lastCount = count;
    return count;
  };
}

// Combo
function Combo(name, products) {
  this.products = ko.observableArray(products);
  this.name = name;

  this.getOldPrice = function () {
    return this.products().reduce(function (total, product) {
      return total + parseFloat(product.price() * 1);
    }, 0);
  };
  this.getPrice = function () {
    return this.getOldPrice() * .75;
  };
}

Combo.prototype = new Product();

// Basket
function Basket() {
  var self = this;
  self.points = ko.observable(245.00);
  self.usedPoints = ko.observable(0);
  self.pointsToUse = ko.observable(0);
  self.pointsToUseValue = ko.computed({
    read: function () {
      return this.pointsToUse().toFixed(2);
    },
    write: function (value) {
      var value = parseFloat(value.replace(/[^\.\d]/g, ""));
      this.pointsToUse(isNaN(value) ? 0 : value);
    },
    owner: this
  });
  self.promoCode = ko.observable('');
  self.totalDisplay = ko.observable(0);
  self.totalDisplay.$animator = $({value: 0});
  self.products = ko.observableArray();
  self.products.subscribe(function () {
    var $button = $('.sidebar-basket-button');
    // points in use
    var delta = self.getTotalPrice();
    if (delta < 0){
      self.usedPoints(self.usedPoints()+delta);
      self.points(self.points()-delta);
    }

    // basket total fx
    self.totalDisplay.$animator.stop(true).animate({
      value: self.getTotalPrice()
    }, {
      duration: 100,
      easing: 'linear',
      step: function (value) {
        self.totalDisplay(value);
      }
    });
    // basket button fx
    $button.addClass('active');
    setTimeout(function () {
      $button.removeClass('active');
    }, 200);
  });
  self.getTemplateName = function (item) {
    if (item.product instanceof Combo) {
      return 'tpl_combo_tablerow';
    }
    if (item.product instanceof Product) {
      return 'tpl_product_tablerow';
    }
    return '';
  };
  self.addProduct = function (product) {
    var price = product.getPrice();
    var basketProduct;
    // clear basket, if it was suspended
    if ((self.getTotalCount() == 0) && (self.getSuspendedCount() > 0)) {
      self.products([]);
    }
    // looking for the same product in basket
    for (var i = 0; i < self.products().length; i++) {
      var p = (self.products())[i];
      if ((p.price() === price) && (p.name === product.name)) {
        basketProduct = p;
        break;
      }
    }
    if (basketProduct) {
      if (basketProduct.suspended()) {
        basketProduct.counter(1);
        self.restoreProduct(basketProduct);
      } else {
        basketProduct.counter(parseInt(basketProduct.counter()) + 1);
      }
    } else {
      basketProduct = $.extend(true, {}, product);
      basketProduct.timestamp = (new Date()).getTime();
      basketProduct.product = product;
      basketProduct.suspended = ko.observable(false);
      basketProduct.price = ko.observable(product.getPrice());
      basketProduct.amount = ko.observable(product.amount());
      basketProduct.counter = ko.observable(0);
      basketProduct.counter.subscribe(function (newValue) {
        basketProduct.getCount();
        if (newValue == 0) {
          basketProduct.suspended(true);
        }
        $('.catalog').each(function () {
          ko.dataFor(this).products.valueHasMutated();
        });
      });
      basketProduct.onRender = function (elements, data) {
        basketProduct.$self = $(elements[0]).parent(); // hack :)
        basketProduct.$self.find('.input-amount .input').input();
        basketProduct.$self.find('.input-amount').input_amount();
        basketProduct.$self.find('.input-amount input').on('change', function () {
          basketProduct.counter(parseInt(basketProduct.$self.find('.input-amount input').val()) || 0);
        });
        basketProduct.$self.find('.input-amount-plus, .input-amount-minus').on('click', function () {
          basketProduct.counter(parseInt(basketProduct.$self.find('.input-amount input').val()) || 0);
        });
      };
      self.products.push(basketProduct);
      basketProduct.counter(1);
    }
    self.products.valueHasMutated();
  };
  self.suspendProduct = function (product) {
    product.suspended(true);
    product.counter.valueHasMutated();
  };
  self.suspendProducts = function () {
    for (var i = 0; i < self.products().length; i++) {
      self.suspendProduct((self.products())[i]);
    }
  };
  self.restoreProduct = function (product) {
    product.suspended(false);
    if (product.counter() <= 0) {
      product.counter(1);
    }
  };
  self.restoreProducts = function () {
    for (var i = 0; i < self.products().length; i++) {
      self.restoreProduct((self.products())[i]);
    }
  };
  self.removeProduct = function (product) {
    var price = product.getPrice();
    for (var i = 0; i < self.products().length; i++) {
      var p = (self.products())[i];
      if (p.price() === price) {
        p.counter(p.counter() - 1);
        if (p.counter() === 0) {
          self.products().splice(i, 1);
        }
        self.products.valueHasMutated();
        return true;
      }
    }
    return false;
  };
  self.getProductsByCategory = function () {
    var products = self.products();
    var productsByCategory = {};
    for (var i = 0; i < products.length; i++) {
      var product = products[i];
      var group = productsByCategory[product.category];
      if (typeof group === 'undefined') {
        group = {
          category: product.category,
          total: 0
        };
        productsByCategory[product.category] = group;
      }
      group.total += product.price;
    }
    return $.map(productsByCategory, function (key, value) {
      return key;
    });
  };
  self.getCountText = function () {
    var count = self.getTotalCount();
    if (count === 0) {
      return 'Корзина пуста';
    } else {
      return 'Всего ' + count + ' ' + util.decl(count, ['продукт', 'продукта', 'продуктов']) + ' на сумму';
    }
  };
  self.getPriceText = function () {
    return util.formatPrice(self.getTotalPrice());
  };
  self.getTotalPrice = function () {
    return self.products().reduce(function (total, product) {
      return total + parseFloat(product.suspended() ? 0 : product.getPrice() * product.counter());
    }, 0) - self.usedPoints();
  };
  self.getTotalCount = function (productName) {
     return self.products().reduce(function (count, product) {
     // use name filter if given
     if (productName && (productName != product.name)) {
     return count;
     }
     return count + parseInt((product.suspended() ? 0 : product.counter()));
     }, 0);
  };
  self.getSuspendedCount = function () {
    return self.products().reduce(function (count, product) {
      return count + parseInt((product.suspended() ? 1 : 0)); // assume suspended product as 1
    }, 0);
  };
  self.getDeliveryCost = function () {
    var total = self.getTotalPrice();
    if (total >= 2000) {
      return 'бесплатно';
    }
    return 'составит 249 <span class="rub">руб.</span>';
  };
  self.usePromo = function () {
  };
  self.usePoints = function () {
     var points_to_use = Math.min(self.getTotalPrice(), self.points(), parseInt(self.pointsToUse()));
     self.usedPoints(self.usedPoints()+points_to_use);
     self.points(self.points()-points_to_use);
  };
  self.sortProducts = function (type, desc) {
    var mod = desc ? +1 : -1;
    var products = this.products().slice(0);
    switch (type) {
      case 'quantity':
      {
        products.sort(function (a, b) {
          return mod * (parseInt(a.getCount()) - parseInt(b.getCount()));
        });
        break;
      }
      case 'timestamp':
      {
        products.sort(function (a, b) {
          return mod * (parseInt(a.timestamp) - parseInt(b.timestamp));
        });
        break;
      }
      case 'price':
      {
        products.sort(function (a, b) {
          return mod * (parseFloat(a.getPrice() * a.getCount()) - parseFloat(b.getPrice() * b.getCount()));
        });
        break;
      }
      case 'name':
      {
        products.sort(function (a, b) {
          return mod * (a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
        });
        break;
      }
      default:
      {
        products.sort(function (a, b) {
          return 0.5 - Math.random();
        });
      }
    }
    self.products(products);
  };
  // auth
  self.toggleBasketWindow = function() {
    alert('1');
  };
}

// Catalog
function CatalogViewModel(source, globalStart, globalLimit, nocombo, search) {
  var self = this;
  self.source = source || '/products';

  self.cache = [];
  self.pages = ko.observableArray();
  self.productsPerPage = ko.observable(Math.floor($('.content').outerWidth() / 230) * 4);
  if (globalLimit && globalLimit < self.productsPerPage()) {
    self.productsPerPage(globalLimit);
  }
  self.currentPage = ko.observable();
  self.products = ko.observableArray();

  self.getTemplateName = function (item) {
    if (item instanceof Combo) {
      return 'tpl_combo_default';
    }
    if (item instanceof Product) {
      return 'tpl_product_default';
    }
    return '';
  };

  self.loadProductsData = function (start, limit, page) {
    $.getJSON(self.source, {start: start, limit: limit}, function (data) {
      var products = $.map(data.products, function (item) {
        item.image += ($('html').is('.ie8') ? '?'+Math.random() : ''); // stupid ie8 hack
        if (search) {
          item.name = item.name.replace(search, '<span class="highlight">'+search+'</span>');
        }
        return new Product(item);
      });
      // update pages if this is the first AJAX response
      if (self.cache.length === 0) {
        var total = Math.ceil(data.total / self.productsPerPage());
        self.pages([]);
        for (var i = 0; i < total; i++) {
          self.pages.push(i + 1);
        }
      }
      // inject combo
      if (!nocombo && (products.length>4)) {
        var combo = new Combo('Выгодное предложение #'+page, [products[1], products[2], products[3], products[4]]);
        products.splice(2, 0, combo);
      }
      // update timestamp
      $.map(products, function (product, index) {
        product.timestamp = 1000 - index;
      });
      self.cache[page] = products;
      self.products(products);
    });
  };

  self.getProductsData = function(page) {
    if (self.cache[page]) {
      self.products(self.cache[page]);
    } else {
      var limit = globalLimit ? globalLimit : self.productsPerPage();
      var start = globalStart ? globalStart : (page - 1) * limit + 1;
      self.loadProductsData(start, limit, page);
    }
  };

  self.productsPage = function (page) {
    self.currentPage(page);
    self.getProductsData(page);
    if ((page > 1) || (window.location.hash != '')) {
      window.location.hash = page;
    }
    $('body, html').animate({scrollTop: 0});
  };

  self.getNextPage = function () {
    return Math.min(self.currentPage() + 1, self.pages().length);
  };

  self.sortProducts = function (type, desc) {
    var mod = desc ? +1 : -1;
    var products = self.products().slice(0);
    switch (type) {
      case 'quantity':
      {
        products.sort(function (a, b) {
          return mod * (parseInt(a.getCount()) - parseInt(b.getCount()));
        });
        break;
      }
      case 'timestamp':
      {
        products.sort(function (a, b) {
          return mod * (parseInt(a.timestamp) - parseInt(b.timestamp));
        });
        break;
      }
      case 'price':
      {
        products.sort(function (a, b) {
          return mod * (parseFloat(a.getPrice()) - parseFloat(b.getPrice()));
        });
        break;
      }
      case 'rating':
      {
        products.sort(function (a, b) {
          return mod * (parseFloat(a.rating) - parseFloat(b.rating));
        });
        break;
      }
      case 'new':
      {
        products.sort(function (a, b) {
          return mod * (parseFloat(a.newness) - parseFloat(b.newness));
        });
        break;
      }
      default:
      {
        products.sort(function (a, b) {
          return 0.5 - Math.random();
        });
      }
    }
    self.products(products);
  };

  self.getTotal = function() {
    return self.products().reduce(function (total, product) {
      return total + parseFloat(product.getPrice());
    }, 0);
  };

  self.updateControls = function (elements, data) {
    self.$self = $(elements[0]);
    $(elements).find('.input-amount .input').input();
    $(elements).find('.input-amount').input_amount();
    $(elements).find('.switcher').switcher().on('switch.switcher', function (event, item) {
      var target = (event.currentTarget) ? event.currentTarget : event.srcElement;
      var product = ko.dataFor(target);
      product.updateSwitcher(item);
    });

    if(self.products.indexOf(data) == self.products().length - 1) {
      $(elements).closest('.products').trigger('gridComplete');
    }
  };
  var page = parseInt(window.location.hash.substr(1)) || 1;
  self.productsPage(page);
}

(function ($, window, document, undefined) {
  'use strict';

  $(function () {
    window.abv.basket = new Basket();
    $('.basket').each(function () {
      ko.applyBindings(window.abv.basket, this);
    });
    $('.catalog').each(function () {
      var source = $(this).data('source');
      var start = $(this).data('start');
      var limit = $(this).data('limit');
      var nocombo = $(this).data('nocombo');
      var search = $(this).data('search');
      ko.applyBindings(new CatalogViewModel(source, start, limit, nocombo, search), this);
    });

    // filters
    $('.catalog-filters').on('click', '.itemset-item', function () {
      var $filter = $(this);
      if ($filter.is('.with-icon_down')) {
        $filter.removeClass('with-icon_down').addClass('with-icon-right with-icon-white with-icon_up');
      } else {
        $filter.removeClass('with-icon_up').addClass('with-icon-right with-icon-white with-icon_down');
      }
      $filter.siblings().removeClass('with-icon-right with-icon-white with-icon_down with-icon_up');
      var sort = ($filter.is('.with-icon_down') || $filter.is('.with-icon_up')) ? $filter.data('sort') : $filter.data('sort') ? 'timestamp' : undefined;
      var $catalog = $(this).closest('.catalog');
      if ($catalog.length === 0) {
        $catalog = $('.catalog.personal_products__shelf:not(.hidden)').first();
      }
      ko.dataFor($catalog.get(0)).sortProducts(sort, $filter.is('.with-icon_down'));
    });

  });

})(jQuery, window, document);