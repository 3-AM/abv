;(function($, window, document, undefined) {
  'use strict';

  $(function() {
    window.abv = window.abv || {};

	$('.top').affix();

    // header search example
    $('.search-example').on('click', function() {
      $('.search-input').input('val', $(this).text());
    });

    $('.fold').foldable();
    $('.footnote').footnote();
    $('.itemset:not(.notApplyPlugin)').itemset();
    $('.itemset-new:not(.notApplyPlugin)').itemset_new();
    $('.tabs').tabs().on('click', '.tab-header', function() {
      $('.sidebar-contents').find('.sidebar-content').eq($(this).index()).show().siblings().hide();
    });
    $('select').chosen({disable_search: true});

    $('.switcher:not(.notApplyPlugin)').switcher();
    $('.switcher_simple:not(.notApplyPlugin)').switcher_simple();
    $('.input:not(.notApplyPlugin)').input();
    $('.input-amount').input_amount();
    $('.radioitem').radioitem();
    $('.popup-modal').popup_modal();

    // masked input
    abv.mask = {
      'phone': '+7? 999 999 99 99',
      'number_natural': '9?9999'
    };

    abv.caretFix = function(input){ //костыль, ставим каретку не дальше введенных данных
      var value = $.trim(input.value);
      if(value  == '+7') value += ' ';
      var caret = $(input).caret();
      var begin = caret.begin;
      var end = caret.end;

      if(end > value.length){
        setTimeout(function(){
          $(input).caret(value.length);
        }, 0);
      }
    };

    $('.mask-phone input').val('+7').mask(abv.mask.phone, {placeholder: ''})
      .on('click', function(){
        abv.caretFix(this);
      });
    $('.mask-number-natural input').mask(abv.mask.number_natural, {placeholder: ''});

    // popups
    var close_popups = function() {
      $('.popups-fader').hide();
      $('.popups').hide().find('.popup').hide();
      $('.sidebar-login-window').hide();
      $('.header-search').removeClass('header-search-fade');
      $('body').css('overflow', '');
    }
    $('.popup-close').on('click', close_popups);
    $(document).on('click', close_popups);
    $(document).on('keydown', function(event) {
      if (event.keyCode == $.ui.keyCode.ESCAPE) {
        close_popups();
      }
    });

  });

  // ToDo: disable less caching, this line should be removed
  localStorage.clear();

})(jQuery, window, document);