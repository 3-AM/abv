$(function(){

  /* itemScroller */
  (function(){
    var $scroller = $(".itemScroller");
    if(!$scroller.length) return;

    var $arrows = $scroller.find(".arrow");
    var $left = $arrows.filter(".left");
    var $right = $arrows.filter(".right");
    var $inner = $scroller.find(".inner");
    var $outer = $scroller.find(".outer");

    var item_duration = 400;
    var item_easing = "easeOutQuint";
    var item_delta = 10;

    // ховер
    $scroller.find(".item")
      .on("mouseenter", function(){
        $(this).stop().animate({
          "top": -item_delta+"px"
        }, item_duration, item_easing);
      })
      .on("mouseleave", function(){
        $(this).stop().animate({
          "top": 0
        }, item_duration, item_easing);
      });

    // скроллинг
    (function(){
      var scroll_delta = 440;
      var scroll_duration = 460;
      var scroll_easing = "easeOutQuad";

      var getPos = function(){
        return parseInt($inner.css("left"), 10);
      };

      var getInnerWidth = function(){
        return $inner.width();
      };

      var getOuterWidth = function(){
        return $outer.width();
      };

      var refreshArrows = function(){
        var pos = getPos();
        var innerWidth = getInnerWidth();
        var outerWidth = getOuterWidth();

        $left.toggle(pos < 0);
        $right.toggle(pos + innerWidth > outerWidth);
      };

      var scroll = function(newPos, isJump){
        $inner.animate({
          "left": newPos + "px"
        },{
          "duration": isJump ? 0 : scroll_duration,
          "easing": scroll_easing,
          "complete": function(){
            refreshArrows();
          }
        });
      };


      $(window).on("resize", function(){
        var pos = getPos();
        var innerWidth = getInnerWidth();
        var outerWidth = getOuterWidth();

        if(innerWidth <= outerWidth){
          scroll(0, true);
        }else if(innerWidth + pos < outerWidth){
          scroll(outerWidth - innerWidth, true);
        }

        refreshArrows();
      });

      $arrows.click(function(e){
        var isLeft = $(e.target).hasClass("left");
        var pos = getPos();
        var innerWidth = getInnerWidth();
        var outerWidth = getOuterWidth();

        var left_lim = 0;
        var right_lim = outerWidth - innerWidth;

        if(isLeft){
          var to_pos = pos + scroll_delta;
          if(to_pos > left_lim) to_pos = left_lim;
        }else{
          var to_pos = pos - scroll_delta;
          if(to_pos < right_lim) to_pos = right_lim;
        }

        scroll(to_pos);
      });

      //init
      refreshArrows();
    })();
  })();
});

/* wallet size */
function showWallet(sum){
var $ws = $("<span />")
  .addClass("walletSize")
  .html(util.formatPrice(sum));

$(".submenu [data-name=wallet]:not(.selected)").append($ws);
}