$(function(){
  'use strict';

  /* randomProduct */
  (function(){
    var $products = $(".randomProduct");
    var index = util.getRandomInt(0, $products.length - 1);
    $products.eq(index).show();

    // year selector
    $('.personal_wallet_years').on('click', '.itemset-item', function() {
      var year = $(this).text();
      $('.personal_wallet_orderlines').find('.orderline[data-year="'+year+'"]').show().siblings().hide();
    });

  })();

});