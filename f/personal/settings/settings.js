$(function(){
  'use strict';

  var duration = 200;
  var duration_button = 300;

  var delay_saveRequest = 1300;
  var delay_done = 1000;
  var duration_save = 180;


  // ===========
  // == АДРЕС ==
  // ===========
  (function(){
    var $form = $('.form-address');
    var $submit = $form.find('.button-green');
    var formSTATE = '';

    var selectorFields = 'input[name="addressName[]"], input[name="street[]"], input[name="building[]"]';

    // save
    var $submitRow = $form.find('.form-row-button');
    var checkSTATE = function(init){
      if($form.hasClass('changed')) return;
      var res = '';
      $form
        .find(selectorFields + ', input[name="building2[]"], input[name="building3[]"], input[name="apartment[]"]')
        .each(function(){
          res += this.value;
        });

      if(res != formSTATE){
        formSTATE = res;
        if(!init){
          $form.addClass('changed');
          $submitRow.slideDown(duration_button);
        }
      }
    };


    $submitRow.find('button').click(function(){
      var $button = $(this);
	  var $submitRow = $(this).parents('.form-row-button');
      var $button_add = $('.personal_settings__addAddressLink');
      var $link_remove = $('.personal_settings__removeAddressLink');
      var $elements = $form.find('input').not($button);
      var $done = $form.find('.done');

      $button
        .addClass('preloader')
        .attr('disabled', 'disabled');
      $elements
        .attr('readonly', 'readonly');
      $button_add.add($link_remove).addClass('disabled');

      $form.removeClass('changed');
	  
	  $.post($form.attr('action'), $form.serialize(), function(data){
        $button.stop().fadeOut(duration_save, function(){
          $done.stop().fadeIn(duration_save, function(){
            setTimeout(function(){
              $submitRow.stop().slideUp(duration_button, false, function(){
                $button.stop().fadeIn(0);
                $done.stop().fadeOut(0);

                $button
                  .removeClass('preloader')
                  .removeAttr('disabled');
                $elements
                  .removeAttr('readonly');
                $button_add.add($link_remove).removeClass('disabled');
                checkSTATE(true);
            }, delay_done);
          });
        });
	  });
	  

      });
    });

    var changeStateHandler = function(e, isValid){
      if(!$(this).data('popup')) return;

      if(isValid){
        $(this).popup('hide');
      }else{
        $(this).popup('show');
      }
    };

    var Address = function(editNameMode, name, street, building, building2, building3, apartment){
      this.editNameMode = ko.observable(editNameMode);
      this.name = ko.observable(name);
      this.street = street || '';
      this.building = building || '';
      this.building2 = building2 || '';
      this.building3 = building3 || '';
      this.apartment = apartment || '';
    };

    var AddressViewModel = function(data){
      var t = this;

      data = $.map(data, function(address){
        return new Address(
          address.editNameMode,
          address.name,
          address.street,
          address.building,
          address.building2,
          address.building3,
          address.apartment
        );
      });

      t.data = ko.observableArray(data);

      t.init = function(elements, data){
        $(elements)
          .find('.input').input()
          .end()
          .find('[name="addressName[]"]').focus().select();

        $form.find(selectorFields).on('changeState', changeStateHandler);
        if(typeof(validator) != 'undefined') validator.refresh(true);
      };

      t.add = function(data, event){
        var $elem = $(event.target);
        if($elem.hasClass('disabled')) return;
        var address = new Address(true, 'еще один мой адрес');
        t.data.push(address);

        //focus
        var $fields = $elem.closest('.form-address').find('[name="addressName[]"]');
        if($fields.length > 1){
          $fields.last().focus().select();
        }
      };

      t.remove = function(address, event){
        if($(event.target).hasClass('disabled')) return;
        t.data.remove(address);
      };

      t.show = function(elem){
        $(elem).hide().slideDown(duration);
      };

      t.hide = function(elem){
        $(elem).slideUp(duration, function(){
          $(elem).remove();
          if(typeof(validator) != 'undefined') validator.refresh(true);
          checkSTATE();
        });
      };
    };

    ko.applyBindings(new AddressViewModel(abv.address_data), $('.form-address').get(0));




    // валидация
    $form.on('changeState', function(e, isChanged, isValid, remainingFields){
      if(isChanged){
        if(isValid){
          $submit.removeAttr('disabled');
        }else{
          $submit.attr('disabled', 'disabled');
        }
      }
    });


    var options = {
      'selector': selectorFields,
      'fields': {
        'addressName[]': { 'validatorFuncName': 'noempty' },
        'street[]': { 'validatorFuncName': 'noempty' },
        'building[]': { 'validatorFuncName': 'noempty' }
      }
    };
    var validator = new Validator($form, options);





    checkSTATE(true);

    $form.on('input keyup', function(){
      checkSTATE();
    });

  })();




  // ================
  // == ПОЛУЧАТЕЛЬ ==
  // ================
  (function(){
    var $form = $('.form-recipient');
    var $submit = $form.find('.button-green');
    var formSTATE = '';
    var selectorFields = 'input[name="name[]"], input[name="phone[]"], input[name="email[]"]';

    var changeStateHandler = function(e, isValid){
      if(!$(this).data('popup')) return;

      if(isValid){
        $(this).popup('hide');
      }else{
        $(this).popup('show');
      }
    };


    // save
    var $submitRow = $form.find('.form-row-button');
    var checkSTATE = function(init){
      if($form.hasClass('changed')) return;
      var res = '';
      $form.find(selectorFields).each(function(){
        res += this.value;
      });

      $form.find('.switcher_simple').each(function(){
        res += $(this).hasClass('checked');
      });

      if(res != formSTATE){
        formSTATE = res;
        if(!init){
          $form.addClass('changed');
          $submitRow.slideDown(duration_button);
        }
      }
    };


    $submitRow.find('button').click(function(){
      var $button = $(this);
	  var $submitRow = $(this).parents('.form-row-button');
      var $button_add = $('.personal_settings__addRecipientLink');
      var $link_remove = $('.personal_settings__removeRecipientLink');
      var $switcher = $form.find('.switcher_simple');
      var $elements = $form.find('input').not($button);
      var $done = $form.find('.done');


      $button
        .addClass('preloader')
        .attr('disabled', 'disabled');
      $elements
        .attr('readonly', 'readonly');
      $switcher.switcher_simple('disabled', true);
      $button_add.add($link_remove).addClass('disabled');

      $form.removeClass('changed');
	  
	  $.post($form.attr('action'), $form.serialize(), function(data){
        $button.stop().fadeOut(duration_save, function(){
          $done.stop().fadeIn(duration_save, function(){
            setTimeout(function(){
              $submitRow.stop().slideUp(duration_button, false, function(){
                $button.stop().fadeIn(0);
                $done.stop().fadeOut(0);

                $button
                  .removeClass('preloader')
                  .removeAttr('disabled');
                $elements
                  .removeAttr('readonly');
                $switcher.switcher_simple('disabled', false);
                $button_add.add($link_remove).removeClass('disabled');

                checkSTATE(true);
            }, delay_done);
          });
        });
	  });

      });
    });


    var Recipient = function(name, phone, email, subscription){
      this.name = name || '';
      this.phone = phone || '+7';
      this.email = email || '';
      this.subscription = ko.observable(subscription || '');
    };

    var RecipientViewModel = function(data){
      var t = this;

      data = $.map(data, function(recipient){
        return new Recipient(
          recipient.name,
          recipient.phone,
          recipient.email,
          recipient.subscription
        );
      });

      t.data = ko.observableArray(data);

      t.init = function(elements, data){
        $(elements)
          .find('[name="phone[]"]').mask(abv.mask.phone, {placeholder: ''}).on('click', function(){
            abv.caretFix(this);
          })
          .end()
          .find('.input').input()
          .end()
          .find('.switcher_simple')
            .switcher_simple()
            .on('switch', function(){
				if ($(this).hasClass('checked')) {
					$(this).siblings('input[name="subscription[]"]').val(1);
				}
				else {
					$(this).siblings('input[name="subscription[]"]').val(0);
				}
              $('.form-recipient').trigger('input');
            });

        $form.find(selectorFields).on('changeState', changeStateHandler);
        if(typeof(validator) != 'undefined') validator.refresh(true);
      };

      t.add = function(data, event){
        var $elem = $(event.target);
        if($elem.hasClass('disabled')) return;
        var recipient = new Recipient();
        t.data.push(recipient);

        //focus
        var $fields = $elem.closest('.form-recipient').find('[name=name]');
        if($fields.length > 1){
          $fields.last().focus().select();
        }
      };

      t.remove = function(address, event){
        if($(event.target).hasClass('disabled')) return;
        t.data.remove(address);
      };

      t.show = function(elem){
        $(elem).hide().slideDown(duration);
      };

      t.hide = function(elem){
        $(elem).slideUp(duration, function(){
          $(elem).remove();
          if(typeof(validator) != 'undefined') validator.refresh(true);
          checkSTATE();
        });
      };
    };
	
    ko.applyBindings(new RecipientViewModel(abv.user_data), $('.form-recipient').get(0));


    // валидация

    var options = {
      'selector': selectorFields,
      'fields': {
        'name[]': { 'validatorFuncName': 'noempty' },
        'phone[]': { 'validatorFuncName': 'phone' },
        'email[]': { 'validatorFuncName': 'email' }
      }
    };
    var validator = new Validator($form, options);

    $form.on('changeState', function(e, isChanged, isValid, remainingFields){
      if(isChanged){
        if(isValid){
          $submit.removeAttr('disabled');
        }else{
          $submit.attr('disabled', 'disabled');
        }
      }
    });





    checkSTATE(true);

    $form.on('input keyup', function(){
      checkSTATE();
    });
  })();


  // ==================
  // == СМЕНА ПАРОЛЯ ==
  // ==================
  (function(){
    var $form = $('.form-password');
    var $save = $form.find('.form-row-button');
    var $pass_old = $form.find('.name-password_old');
    var $pass_new = $form.find('.name-password_new');
    var $submit = $form.find('button');
    var selectorFields = 'input[name=password_old], input[name=password_new]';

    $form.on('input keyup', function(){
      var val_old = $pass_old.find('input').val();
      var val_new = $pass_new.find('input').val();

      if(val_old && val_new){
        if(!$form.hasClass('changed')){
          $form.addClass('changed');
          $save.stop().slideDown(duration_button);
        }

      }
    });

    $submit.click(function(){
      var $button = $(this);
      var $elements = $form.find('input').not($button);
      var $done = $form.find('.done');

      $button
        .addClass('preloader')
        .attr('disabled', 'disabled');
      $elements
        .attr('readonly', 'readonly');

      $form.removeClass('changed');

	  $.post($form.attr('action'), $form.serialize(), function(data){
        $button.stop().fadeOut(duration_save, function(){
          $done.stop().fadeIn(duration_save, function(){
            setTimeout(function(){
              $pass_old.add($pass_new).input('val', '');

              $save.stop().slideUp(duration_button, function(){
                $button.stop().fadeIn(0);
                $done.stop().fadeOut(0);

                $button
                  .removeClass('preloader')
                  .removeAttr('disabled');
                $elements
                  .removeAttr('readonly');
              });
            }, delay_done);
          });
        });
	  });
    });


    // валидация

    var options = {
      'selector': selectorFields,
      'fields': {
        'password_old': { 'validatorFuncName': 'noempty' },
        'password_new': { 'validatorFuncName': 'noempty' }
      }
    };
    var validator = new Validator($form, options);

    $form.on('changeState', function(e, isChanged, isValid, remainingFields){
      if(isChanged){
        if(isValid){
          $submit.removeAttr('disabled');
        }else{
          $submit.attr('disabled', 'disabled');
        }
      }
    });
  })();


  // Показать пароль
  (function(){
    var $form = $('.form-password');
    var $pass_old = $form.find('.name-password_old');
    var $pass_new = $form.find('.name-password_new');
    var $showpass = $form.find('.form-password-showpass');

    $showpass.on('click', function(){
      $(this).toggleClass('mode-hidepass');
      $pass_old.add($pass_new).toggleClass('password');
    });
  })();

});