$(function(){

  var duration_scroll = 800;
  var duration_open = 500;
  var duration_close = 450;
  var easing_open = 'easeInSine';
  var easing_close = 'easeOutSine';



  // открываем заказ по номеру
  /*
  (function(){
    var $order = $('.order.old:first');
    var $header = $order.find('.order_header');

    $order.find('.order_products .catalog .products').on('gridComplete', function(){
      if(window.location.search){
        $header.trigger('click');
        $('html,body').delay(100).animate({scrollTop: Math.round($order.offset().top)}, duration_scroll);
      }
    });
  })();
  */

  // "Оставить отзыв"

  $('.button_feedback').on('click', function(event) {
      $('.feddback-side-content').css('top', 0);
      $('.popups-fader').show();
      $('.popups').show().find('.feedback-popup').show();
      $('body, html').animate({scrollTop: 0}, 0);
      $('body').css('overflow', 'hidden');
      event.stopPropagation();
    });


    $('.feedback-popup-wrapper').on('click', function(event) {
      event.stopPropagation();
    });

  // "Повторить заказ"
  $('.button_repeat').click(function(){
    util.redirect('/order/');
  });


  // открыть/закрыть заказ
  (function(){


    var animParams = {
      'closed': {
        'left': 0,
        'right': 0,
        'radius': 7
      },

      'opened': {
        'left': -32,
        'right': -35,
        'radius': 0
      }
    };

    $('.order_header').on('click', function(e){
      var $order = $(this).closest('.order');
      var $bg = $order.find('.order_header_bg');
      var $products = $order.find('.order_products');
      var $catalog = $products.find('.catalog');
      var $controls = $products.find('.order_products_controls');
      var isOpened = $order.hasClass('opened');

      $order.toggleClass('opened');


      var height = ($controls.length ? $controls.outerHeight() : 0)
      + $catalog.outerHeight();

      if(isOpened){ //закрыть
        $products
          .stop()
          .css({
            'overflow': 'hidden'
          })
          .animate(
          {
            'height': 0
          },
          {
            'duration': duration_close,
            'easing': easing_close
          }
        );

        $bg.stop().animate({
          'left': animParams.closed.left + 'px',
          'right': animParams.closed.right + 'px',
          'border-radius': animParams.closed.radius + 'px'
        },{
          'duration': duration_close,
          'easing': easing_close
        });
      }else{ //открыть
        $products
          .stop()
          .css({
            'overflow': 'hidden'
          })
          .animate(
          {
            'height': height + 'px'
          },
          {
            'duration': duration_open,
            'easing': easing_open,
            'complete': function(){
              $(this).css({
                'overflow': 'visible',
                'height' : 'auto'
              });
            }
          }
        );

        $bg.stop().animate({
          'left': animParams.opened.left + 'px',
          'right': animParams.opened.right + 'px',
          'border-radius': animParams.opened.radius + 'px'
        },{
          'duration': duration_open,
          'easing': easing_open
        });
      }
    });
  })();









});