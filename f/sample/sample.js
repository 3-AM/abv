$(function(){
  'use strict';

  $('.sample__showPassword').on('click', function() {
    $(this).toggleClass('showpass');
    $('.sample-pass').toggleClass('password');
  });

  // popup modal
  $(".sample__popup_modal_link").click(function(){
    $(".sample__popup_modal").popup_modal("open");
  });
});