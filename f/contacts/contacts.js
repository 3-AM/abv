$(function () {
  "use strict";


  var $submit = $(".send_message");
  var $form = $(".contacts_form");
  var $all_fields = $form.find("input, textarea");

  var $name = $form.find(".name input");
  var $phone = $form.find(".phone input");
  var $email = $form.find(".email input");
  var $message = $form.find(".message");
  var $captcha = $(".itemset-captcha");

  var $captcha_error = $(".captcha-error");


  if(window.location.hash == '#feedback'){
    util.scrollTo($form, {
      'duration': 800
    });
  }


  // yandex maps
  (function(){
    var map;
    var placemark;
    var zoom = 10;

    // init map
    ymaps.ready(function () {
      placemark = new ymaps.Placemark([55.8, 37.6], {
        hintContent: 'Центральный офис',
        balloonContentHeader: '<h5 class="balloonTitle">Центральный офис</h5>',
        balloonContentBody:
          '<b>Адрес:</b> Анненский пр., д. 1, стр. 20, Москва<br/>'+
          '<b>Телефон:</b> +7 499 707-07-33<br/>'+
          '<b>Электронная почта:</b> <a href="info@abv-store.ru">info@abv-store.ru</a><br/>'
      }, {
        iconImageHref: '/f/placemark.png',
        iconImageSize: [46, 47],
        iconImageOffset: [-15, -43]
      });

      map = new ymaps.Map('map', {
        center: placemark.geometry.getCoordinates(),
        zoom: zoom,
        behaviors: ['default', 'scrollZoom']
      });

      map.controls.add('mapTools');
      map.controls.add('zoomControl');
      map.controls.add('scaleLine');

      map.geoObjects.add(placemark);
    });

    $(".contacts_address_link").click(function () {
      map.setCenter(placemark.geometry.getCoordinates(), zoom);
    });
  })();


  // Валидация формы
  (function(){
    var selectorFields = 'input[name], textarea[name], .itemset-captcha';
    var options = {
      'selector': selectorFields,

      'fields': {
        'name': { 'validatorFuncName': 'noempty' },
        'phone': { 'validatorFuncName': 'phone' },
        'email': { 'validatorFuncName': 'email' },
        'message': { 'validatorFuncName': 'noempty' },
        'captcha': {
          'validatorFuncName': 'captcha',
          'eventShow': 'changeitem',
          'eventValidate': 'changeitem'
        }
      }
    };

    var validator = new Validator($form, options);

    $form.on('changeState', function(e, isChanged, isValid, remainingFields){
      if(isChanged){
        if(isValid){
          $submit.removeAttr('disabled');
        }else{
          $submit.attr('disabled', 'disabled');
        }
      }
    });

    $(selectorFields).on('changeState', function(e, isValid){
      if(!$(this).data('popup')) return;

      if(isValid){
        $(this).popup('hide');
      }else{
        $(this).popup('show');
      }
    });
  })();




  // капча и сабмит
  (function(){
    var delay_server = 2000;

    var data = [
      {
        "question": "На какой из этих картинок изображено яблоко?",
        "images": ["/f/captcha-1.png", "/f/captcha-2.png", "/f/captcha-3.png", "/f/captcha-4.png", "/f/captcha-5.png"],
        "right_answer": 2
      },

      {
        "question": "На какой из этих картинок изображен арбуз?",
        "images": ["/f/captcha-5.png", "/f/captcha-4.png", "/f/captcha-1.png", "/f/captcha-2.png", "/f/captcha-3.png"],
        "right_answer": 0
      },

      {
        "question": "На какой из этих картинок изображены огурцы?",
        "images": ["/f/captcha-4.png", "/f/captcha-3.png", "/f/captcha-2.png", "/f/captcha-1.png", "/f/captcha-5.png"],
        "right_answer": 3
      }
    ];

    var VM = function(data){
      var t = this;
      t.data = data;

      t.id = ko.observable(0);

      /**
       * рефреш капчи
       */
      t.next = function(){
        var id_new = t.id()+1;
        if(id_new >= t.data.length){
          t.id(0);
        }else{
          t.id(id_new);
        }
      };

      t.applyPlugins = function(elements, d){
        $(elements).closest(".itemset").itemset();
      };

      t.question = ko.computed(function(){
        return t.data[t.id()].question;
      });


      t.items = ko.computed(function(){
        return t.data[t.id()].images;
      });

      t.submit = function(){
        var $items = $captcha.find(".itemset-item");
        var answer_index = $items.index($items.filter(".selected"));
        var right_answer_index = t.data[t.id()].right_answer;

        $captcha_error.css("visibility", "hidden");

        if(right_answer_index == answer_index){ // капча пройдена
          t.waitingOn();
		  $.post('/ajax/feedback.php', $('#contacts_form').serialize(), function(data){
			$('.tab-content').html('Спасибо, что воспользовались формой обратной связи на&nbsp;нашем сайте. Ваше письмо будет рассмотрено в&nbsp;кратчайшие сроки. Ответ придет на&nbsp;указанный электронный адрес.');
		  });
        }else{ // капча не пройдена
          t.waitingOn();
          setTimeout(function(){
            t.waitingOff();
            $captcha.find(".itemset-item.selected").removeClass("selected");
            t.next();

            //показываем текст ошибки
            $captcha_error.css("visibility", "visible");

            // пинаем валидатор
            $form.data('validator-instance').refresh(true);

          }, delay_server);
        }
      };

      t.disableForm = function(){
        //$all_fields.attr("disabled", "disabled");
        $captcha.find(".itemset-item").addClass("disabled");
      };

      t.enableForm = function(){
        //$all_fields.removeAttr("disabled");
        $captcha.find(".itemset-item").removeClass("disabled");
      };

      t.waitingOn = function(){
        t.disableForm();
        $submit.attr("disabled", "disabled").addClass("preloader");
      };

      t.waitingOff = function(){
        t.enableForm();
        $submit.removeAttr("disabled").removeClass("preloader");
      };

    };

    var feedbackVM = new VM(data);

    ko.applyBindings(feedbackVM, $(".form-row-captcha").get(0));

    // submit
    $submit.click(function(){
      feedbackVM.submit();
    });
  })();






});



