$(function() {
  'use strict';

  (function(){

    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
      return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
      };
    });

    $('.brand__categories').on('click', '.itemset-item', function() {
      var text = $(this).find('span').text();
      if (text === 'все товары') {
        $('.combo, .product, .product-control, .products-paginator').show();
        return;
      } else {
        $('.combo, .product:not(.brand_logo), .product-control, .products-paginator').hide();
        $('.product:contains(" '+text+'")').show();
      }
    });

  })();

})
