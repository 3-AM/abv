/**
 *  --
 *  solodoff@ya.ru
 *  upd 23.12.2013
 *  --
 *
 *  Валидация форм.
 *
 *  Вешает на поля и на форму классы и триггерит события.
 *
 *  ToDo: более ресурсоемкий, но простой в реализации вариант- события в буфер, по таймеру- если буфер непустой то пробегаемся по всей форме.
 *
 *
 *
 *  Использование:
 *    <form>
 *      <input name="name" data-validator-fn="email" />
 *      <input name="age" />
 *    </form>
 *
 *    var validator = new Validator($('form'), options);
 *
 *
 *
 *
 *  События:
 *    На форме и полях триггерится событие, заданное в eventChangeState:
 *
 *    $(form).on(eventChangeState, function(event, isChanged, isValid, remainingFields){});
 *    $(field).on(eventChangeState, function(event, isValid){});
 *
 *    ,где remainingFields = массив со ссылками на DOM-элементы полей, которые осталось заполнить
 *
 *
 *  Примечание про eventShow(показ валидации):
 *    http://www.artlebedev.ru/tools/technogrette/etc/forms/ (см. 2.Проверка полей)
 *    Если при инициализации поле валидно, то оно автоматом делается show.
 *
 *
 *  Зависимости:
 *    jQuery
 */


/**
 * @param {DOM object} form
 * @param {object} options
 * @constructor
 */
var Validator = function(form, options){
  var self = this;
  self.$form = $(form).first();
  self.options = $.extend({}, Validator.prototype.DEFAULTS, options);

  if(typeof(self.$form.data('validator-instance')) != 'undefined'){
    console.warn('Validator уже был инициализирован для формы:', form);
    return;
  }

  self.$fields = $();

  self._init();
};

Validator.prototype.DEFAULTS = {
  'namespace': 'validator',
  'classForm': 'validator-form',
  'classField': 'validator-field',

  'classInvalid': 'invalid',
  'classValid': 'valid',
  'classShow': 'validation-show', // сразу не пугаем пользователя

  'eventValidate': 'input keyup',
  'eventShow': 'change',

  'eventChangeState': 'changeState', // триггерится на полях и на форме

  'selector': 'input:not([type=submit]), textarea',


  /**
   * Перекрыть настройки для отдельных полей.
   * Перекрыть можно eventValidate, eventShow, а также задать validatorFuncName (если атрибут data-validator-fn не задан)
   *
   * Пример:
   *   var options  = {
   *    'fields': {
   *      'fieldName1': { // атрибут name
   *        'eventShow': 'changeitem', // если пустая строка, то показ валидации включается сразу
   *        'eventValidate': 'changeitem'
   *      }
   *    }
   *  };
   */
  'fields': {},

  /**
   * Объект с ф-циями для валидации.
   *
   * Лучше задавать его глобально, прописав при инициализации валидатора ссылку на него:
   *   var validateFunctions = {...};
   *   new validator($form, {'validateFunctions': validateFunctions});
   *
   * или даже так(после подключения плагина, но перед созданием экземпляра):
   *   Validator.prototype.DEFAULTS.validateFunctions = {...};
   */
  'validateFunctions': {
    'phone': function(field){
      var pattern = /^\+7 \d\d\d \d\d\d-\d\d-\d\d$/gi;
      return pattern.test(field.value);
    },

    'noempty': function(field){
      return field.value !== '';
    },

    'number': function(field){
      var pattern = /^\d+$/gi;
      return pattern.test(field.value);
    }
  }
};

/**
 * Состояние формы или отдельного поля
 * @param {boolean} isValid
 * @param {boolean} isChanged
 * @param {array} remainingFields
 * @constructor
 * @private
 */
Validator.Status_ = function(isValid, isChanged, remainingFields){
  this.isValid = isValid;
  this.isChanged = isChanged;
  this.remainingFields = remainingFields;
};

/**
 * Инициализация
 * @param {boolean} forceValidate перевалидировать старые поля
 * @private
 */
Validator.prototype._init = function(forceValidate){
  var self = this;

  // неймспейсы
  if(!forceValidate){
    self.options.eventValidate = self._addNamespace(self.options.eventValidate);
    self.options.eventShow = self._addNamespace(self.options.eventShow);

    (function(){
      var f = self.options.fields;
      for(var field in f) {
        if (!f.hasOwnProperty(field)) continue;
        if(typeof(field.eventShow) != 'undefined'){
          field.eventShow = self._addNamespace(field.eventShow);
        }

        if(typeof(field.eventValidate) != 'undefined'){
          field.eventValidate = self._addNamespace(field.eventValidate);
        }
      }
    })();
  }


  // находим поля
  var $fields = self.$form.find(self.options.selector)
    .not('[disabled]')
    .not('.disabled');

  var $fieldsToInit = $fields.not('.' + self.options.classField);

  // unbind events
  self._deactivateFields(self.$fields.not($fields));

  self.$fields = $fields;

  // перевалидируем старые поля
  if(forceValidate){
    self._deactivateFields($fields.not($fieldsToInit));
    $fieldsToInit = $fields;
  }

  // инициализируем поля
  $fieldsToInit.each(function(){
    $(this).addClass(self.options.classField);
    self._initField(this);
  });

  // инициализируем форму
  self.$form
    .data('validator-instance', self)
    .addClass(self.options.classForm);
  var statusForm = self._validateForm();
  statusForm.isChanged = true;
  self._formChangeStatus(statusForm);
};

/**
 * Отключает поля от валидации
 * @param {jQuery} $fields
 * @private
 */
Validator.prototype._deactivateFields = function($fields){
  $fields
    .off('.' + this.options.namespace)
    .removeClass(this.options.classField
      + ' ' + this.options.classInvalid
      + ' ' + this.options.classValid
      + ' ' + this.options.classShow);
};

/**
 * Инициализация поля формы
 * @param {DOM object} field
 * @param {boolean} isRevalidate
 * @private
 */
Validator.prototype._initField = function(field, isRevalidate){
  var self = this;
  var $field = $(field);
  var name = $field.attr('name');
  var options = $.extend({}, self.options, self.options.fields[name]);

  /*if(isRevalidate){
   $field.triggerHandler(options.eventValidate);
   return;
   }*/

  var funcName = $field.attr('data-validator-fn') || options.validatorFuncName;
  if(!funcName) return;
  var func = self.options.validateFunctions[funcName];

  var show = function(){
    $field.addClass(options.classShow);
    var status = self._validateField(field, func);
    self._fieldChangeStatus(field, status);
  };

  // валидация поля по событию
  $('body').on(options.eventValidate, $field, function(){
    var status = self._validateField(field, func);
    if(status.isChanged){
      var silent = !$field.hasClass(self.options.classShow);
      self._fieldChangeStatus(field, status, silent);

      var statusForm = self._validateForm();
      self._formChangeStatus(statusForm);
    }
  });

  // show
  var statusField = self._validateField(field, func);
  self._fieldChangeStatus(field, statusField, true);
  $field.one(options.eventShow, function(){
    show();
    var statusForm = self._validateForm();
    self._formChangeStatus(statusForm);
  });
};

/**
 * Валидация поля
 * @param {DOM object} field
 * @param {function} func
 * @returns {Validator.Status_}
 * @private
 */
Validator.prototype._validateField = function(field, func){
  var $field = $(field);
  var isValid = !!func(field);
  var isChanged = isValid != $field.hasClass(this.options.classValid);
  return new Validator.Status_(isValid, isChanged);
};

/**
 * Меняет состояние валидации поля, триггерит событие
 * @param {DOM Object} field
 * @param {Validator.Status_} status
 * @param {boolean} silent не триггерить события
 * @private
 */
Validator.prototype._fieldChangeStatus = function(field, status, silent){
  var $field = $(field);

  $field
    .toggleClass(this.options.classValid, status.isValid)
    .toggleClass(this.options.classInvalid, !status.isValid);

  if(!silent){
    $field.triggerHandler(this.options.eventChangeState, [status.isValid]);
  }
};

/**
 * Меняет состояние валидации формы и триггерит событие
 * @param {Validator.Status_} status
 * @private
 */
Validator.prototype._formChangeStatus = function(status){
  var o = this.options;

  this.$form
    .toggleClass(o.classValid, status.isValid)
    .toggleClass(o.classInvalid, !status.isValid)
    .triggerHandler(o.eventChangeState, [status.isChanged, status.isValid, status.remainingFields]);
};

/**
 * Валидация формы
 * @returns {Validator.Status_}
 * @private
 */
Validator.prototype._validateForm = function(){
  var self = this;
  var o = self.options;
  var isValid = true;
  var remainingFields = [];

  self.$fields.each(function(){
    var $field = $(this);
    if($field.hasClass(o.classValid)) return;
    isValid = false;
    remainingFields.push(this);
  });

  var isChanged = isValid != self.$form.hasClass(o.classValid);

  return new Validator.Status_(isValid, isChanged, remainingFields);
};


/**
 * Переинициализировать форму.
 * Если, например, добавились или удалились какие-то поля.
 * Учитывает следующие изменения:
 * - в форму добавили поля
 * - из формы исключили поля
 * - поля сделали disabled(класс или атибут) или наоборот - включили
 *
 * @param {boolean} forceValidate перевалидировать старые поля
 */
Validator.prototype.refresh = function(forceValidate){
  this._init(forceValidate);
};

/**
 * Добавляет неймспейсы к событиям в строке.
 * Пример: 'input keyup' -> 'input.mynamespace keyup.mynamespace'
 * @param {string} event
 * @returns {string}
 * @private
 */
Validator.prototype._addNamespace = function(event){
  return $.trim($.trim(event).replace(/\s+|$/gi, '.' + this.options.namespace + ' '));
};

