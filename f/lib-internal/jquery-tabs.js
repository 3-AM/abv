/*!
 * jQuery Tabs micro plugin
 * Author: Prontiol <prontiol@gmail.com>
 * Licensed under the MIT license
 *
 */
;(function ($, window, document, undefined) {
  'use strict';

  $.fn.extend({
    tabs: function() {
      return this.each(function() {
        var $tabs = $(this);
        var $tabsHeaders = $tabs.find('.tabs-headers');
        var $tabsContents = $tabs.find('.tabs-contents');
        $tabsHeaders.on('click.tabs mouseup.tab', '.tab-header:not(.selected)', function() {
          var $tabHeader = $(this);
          var index = $tabHeader.index();
          $tabHeader.addClass('selected').siblings().removeClass('selected');
          $tabHeader.find('span:not(.tab-header-comment)').toggleClass('link-pseudo');
          $tabHeader.siblings().find('span:not(.tab-header-comment)').toggleClass('link-pseudo');
          $tabsContents.find('.tab-content').eq(index).show().siblings().hide();
        });
      });
    }
  });
})(jQuery, window, document);
