/*
 * --
 * solodoff@ya.ru
 * upd 16.10.2013
 * --
 *
 * <div class="radioitem checked" data-group="sampleItems1">пункт 1</div>
 * <div class="radioitem" data-group="sampleItems1">пункт 2</div>
 *
 * Инициализация:
 * $elem.radioitem({...});
 * 
 */
;(function ($, window, document, undefined){
  var defaults = {
  };
  
  $.fn.extend({
    "radioitem": function(arg){
      var options = $.extend({}, defaults, arg);
      
      return this.each(function(){
        var $elem = $(this);

        var $label = $elem.find("label");
        
        var group = $elem.attr("data-group");
        
        $label.on('click.radioitem', function(){
          if($elem.hasClass("checked")) return;
          $('.radioitem[data-group=' + group + '].checked').removeClass('checked');
          $elem.addClass('checked');
        });
      });
    }
  });
})(jQuery, window, document);
