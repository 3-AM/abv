/*
 *  --
 *  solodoff@ya.ru
 *  upd 20.10.2013
 *  --
 *
 *  Инпут - ввод количества чего-либо, с контролами плюс и минус
 *
 *
 *  Зависимости:
 *  maskedinput
 *  jquery.input by Solodoff
 *
 *
 *  Инициализация:
 *    <div class="input-amount">
 *      <div class="input-amount-minus"/>
 *      <div class="input">...</div>
 *      <div class="input-amount-plus"/>
 *    </div>
 *
 *    $('.input-amount').input_amount();
 *
 *
 */
;(function ($, window, document, undefined){
  var PLUGIN_NAME = "input_amount";

  var defaults = {
  };

  var methods_types = {
  };

  var methods_common = {
  };

  $.fn[PLUGIN_NAME] = function(arg){
    if(typeof(arg) == 'string'){
      var a = Array.prototype.slice.call(arguments, 1);
      var type = methods_types[arg];

      switch(type){
        case "set":
          return this.each(function(){
            $(this).data(PLUGIN_NAME).methods[arg].apply(this, a);
          });

        case "get":
          if(this.length > 1){
            console.warn("Вызов метода '" + arg + "' для нескольких DOM-элементов!"
              + " Отработает метод только для первого элемента из набора.");
          }
          return this.eq(0).data(PLUGIN_NAME).methods[arg].apply(this, a);

        default:
          console.warn("Вызов несуществующего метода '" + arg + "'!");
          return this;
      }
    }
    if(!(typeof(arg) == 'object' || typeof(arg) == 'undefined')) return;

    return this.each(function(){
      var options = $.extend({}, defaults, arg);
      var $elem = $(this);
      /**/
      var $input = $elem.find("input");
      var $inputWrapper = $elem.find(".input");
      var $minus = $elem.find(".input-amount-minus");
      var $plus = $elem.find(".input-amount-plus");

      // focus
      $input
        .on("focus", function(){
          $elem.addClass("focus");
        })
        .on("blur", function(){
          $elem.removeClass("focus");
        });



      $minus.click(function(){
        var val = parseInt($input.val(), 10);
        if(val > 0){
          setValue(val - 1);
        }
        $input.focus();
      });

      $plus.click(function(){
        setValue(parseInt($input.val(), 10) + 1);
        $input.focus();
      });

      var setValue = function(value){
        $inputWrapper.input("val", value);
        $input.trigger('valueUpdated');
      };

      $input.on("blur", function(){
        if($input.val() == ""){
          $inputWrapper.input("val", 0);
        }
      });


      // data
      var methods = {
      };

      $elem.data(PLUGIN_NAME, {
        "methods": $.extend({}, methods, methods_common)
      });

    });
  }
})(jQuery, window, document);
