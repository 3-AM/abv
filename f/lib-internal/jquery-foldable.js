/*!
 * jQuery Foldable text micro plugin
 * Author: Prontiol <prontiol@gmail.com>
 * Licensed under the MIT license
 */
;
(function ($, window, document, undefined) {
  'use strict';

  var defaults = {
    duration: 100,
    easing: 'linear'
  };
  $.fn.extend({
    foldable: function (overrides) {
      var options = $.extend({}, defaults, overrides);
      return this.each(function () {
        var $this = $(this);
        $this
          .on('selectstart', function () {
            return false;
          })
          .on('click.foldable', function () {
            if ($this.is('.with-icon_down')) {
              $this.removeClass('with-icon_down').addClass('with-icon_up');
            } else if ($this.is('.with-icon_up')) {
              $this.removeClass('with-icon_up').addClass('with-icon_down');
            }
            $('.foldable[data-fold="' + $this.data('fold') + '"]').stop(true).slideToggle(options.duration, options.easing, options.callback);
          });
      });
    }
  });
})(jQuery, window, document);