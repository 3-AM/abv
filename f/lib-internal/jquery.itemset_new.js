/*
 *  --
 *  solodoff@ya.ru
 *  upd 16.11.2013
 *  --
 *  itemset с возможностью переключения вьюх
 *
 *  $(elem).itemset_new({...});
 *
 *  <div class="itemset-new">
 *    <span class="itemset-item selected" data-view="view1"><span>первый</span></span>
 *    <span class="itemset-item" data-view="view2"><span>второй</span></span>
 *  </div>
 *
 *  ,где view1 и view2 - классы у вьюх:
 *  <div class="itemset-view view1">контент1</div>
 *  <div class="itemset-view view2 hidden">контент2</div>
 *
 *  Плагин переключает класс hidden
 *
 *
 */
;(function ($, window, document, undefined){
  var PLUGIN_NAME = 'itemset_new';

  var defaults = {
  };

  var methods_types = {
  };

  var methods_common = {
  }



  $.fn[PLUGIN_NAME] = function(arg){
    if(typeof arg == 'string'){
      var a = Array.prototype.slice.call(arguments, 1);
      var type = methods_types[arg];

      switch(type){
        case "set":
          return this.each(function(){
            $(this).data(PLUGIN_NAME).methods[arg].apply(this, a);
          });

        case "get":
          if(this.length > 1){
            console.warn("Вызов метода '" + arg + "' для нескольких DOM-элементов!"
              + " Отработает метод только для первого элемента из набора.");
          }
          return this.eq(0).data(PLUGIN_NAME).methods[arg].apply(this, a);

        default:
          console.warn("Вызов несуществующего метода '" + arg + "'!");
          return this;
      }
    }
    if(!(typeof arg == 'object' || typeof arg == 'undefined')) return;

    return this.each(function(){
      var options = $.extend({}, defaults, arg);
      var $elem = $(this);
      /**/

      var $views = $();
      var $items = $elem.find('.itemset-item');

      $items.each(function(){
        var $item = $(this);
        if($item.attr('data-view')){
          var $view = $('.itemset-view.' + $item.attr('data-view'));
          $views = $views.add($view);
          $item.data('view', $view);
        }
      });

      var refreshViews = function($item){
        var $view = $item.data('view') || $();
        $views.not($view).addClass('hidden');
        $view.removeClass('hidden');
      };

      $items.on('click', function(e){
        if(e.which > 1) return; // 0-ie8 1-норм браузеры
        var $item = $(this);
        if($item.hasClass('selected') || $item.hasClass('disabled')) return;

        $items.not($item).removeClass('selected');
        $item.addClass('selected');
        $elem.trigger('changeitem');
        refreshViews($item);
      });



      // data
      var methods = {
      };

      $elem.data(PLUGIN_NAME, {
        "methods": $.extend({}, methods, methods_common)
        /* ... */
      });
    });
  }
})(jQuery, window, document);
