/*!
 * jQuery Footnote micro plugin
 * Author: Prontiol <prontiol@gmail.com>
 * Licensed under the MIT license
 */
;(function($, window, document, undefined) {
  'use strict';

  var defaults = {
  };
  $.fn.extend({
    footnote: function(overrides) {
      var options = $.extend({}, defaults, overrides);
      return this.each(function() {
        var $this = $(this);
        // creating footnote_popup
        var $footnotePopup = $('<div>')
            .html($this.data('footnote'))
            .addClass('footnote_popup')
            .css({
              display: 'none',
              position: 'absolute',
              top: 0,
              left: 0,
              'z-index': 99
            });
        $('<div>')
            .css({
              position: 'relative',
              display: 'inline-block',
              width: 0,
              height: 0
            })
            .append($footnotePopup)
            .on('click.footnote', function(event) {
              event.stopPropagation();
            })
            .insertAfter($this);
        $this.data('footnote_popup', $footnotePopup);
        // binding events
        $this
            .on('selectstart', function() {
              return false;
            })
            .on('click.footnote', function(event) {
              event.stopPropagation();
              var hideFootnote = function() {
                $footnotePopup.hide();
                $(document).unbind('click.footnote');
                $(document).unbind('keydown.footnote');
              };
              var $footnotePopup = $(this).data('footnote_popup');
              if ($footnotePopup.is(':visible')) {
                hideFootnote();
                return;
              }
              // hide all others
              $('.footnote_popup').hide();
              $footnotePopup.css('display', 'inline-block');
              // hide on click anywhere outside the footnote
              $(document).on('click.footnote', function() {
                hideFootnote();
              });
              // hide on ESC key
              $(document).on('keydown.footnote', function(event) {
                if (event.which === 27) {
                  hideFootnote();
                }
              });
            });
      });
    }
  });
})(jQuery, window, document);
