/*
 *  --
 *  solodoff@ya.ru
 *  upd 4.11.2013
 *  --
 *
 *  Всплывающий элемент
 *
 *
 *
 *
 *
 */
;(function ($, window, document, undefined){
  var PLUGIN_NAME = 'popup';

  var defaults = {
    'content': 'Содержимое всплывающего элемента',
    'extraClass': '',
    'isHidden': false
  };

  var methods_types = {
    'content': 'set',
    'show': 'set',
    'hide': 'set',
    'toggle': 'set',
    'remove': 'set'
  };

  var methods_common = {
  }

  $.fn[PLUGIN_NAME] = function(arg){
    if(typeof arg == 'string'){
      var a = Array.prototype.slice.call(arguments, 1);
      var type = methods_types[arg];

      switch(type){
        case 'set':
          return this.each(function(){
            $(this).data(PLUGIN_NAME).methods[arg].apply(this, a);
          });

        case 'get':
          if(this.length > 1){
            console.warn("Вызов метода '" + arg + "' для нескольких DOM-элементов!"
              + " Отработает метод только для первого элемента из набора.");
          }
          return this.eq(0).data(PLUGIN_NAME).methods[arg].apply(this, a);

        default:
          console.warn("Вызов несуществующего метода '" + arg + "'!");
          return this;
      }
    }
    if(!(typeof arg == 'object' || typeof arg == 'undefined')) return;

    return this.each(function(){
      var options = $.extend({}, defaults, arg);
      var $elem = $(this);
      /**/

      var $body = $('<div />')
        .addClass('popup-body')
        .css({
          'position': 'absolute',
          'left': 0,
          'top': 0
        })
        .append($elem.attr('data-popup-content') || options.content);

      var $wrapper = $('<div />')
        .addClass('popup-wrapper')
        .addClass(options.extraClass)
        .css({
          'position': 'relative',
          'display': 'inline-block',
          'width': 0,
          'height': 0
        })
        .append($body.hide())
        .insertBefore($elem);

      //init
      if(!options.isHidden){
        $body.show();
      }


      // data
      var methods = {
        'content': function(content){
          $body.empty().append(content);
        },

        'show': function(){
          if(!$body.is(':hidden')) return;
          $body.show();
          $elem.trigger('show.popup');
        },

        'hide': function(){
          if($body.is(':hidden')) return;
          $body.hide();
          $elem.trigger('hide.popup');
        },

        'toggle': function(){
          if($body.is(':hidden')){
            methods.show();
          }else{
            methods.hide();
          }
        },

        'remove': function(){
          $elem
            .off('.popup')
            .removeData('popup');

          $wrapper.remove();
        }
      };

      $elem.data(PLUGIN_NAME, {
        'methods': $.extend({}, methods, methods_common),
        'wrapper': $wrapper
      });
    });
  }
})(jQuery, window, document);
