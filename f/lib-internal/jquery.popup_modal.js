/*
 *  --
 *  solodoff@ya.ru
 *  upd 23.10.2013
 *  --
 *
 *  Модальное окно
 *
 *  Зависимости:
 *
 *  Инициализация:
 *    $elem.popup_modal({...});
 *
 *  События:
 *
 *  Методы:
 *
 */
;(function ($, window, document, undefined){
  var PLUGIN_NAME = "popup_modal";

  var defaults = {
    "duration": 300,
    "content": undefined
  };

  var methods_types = {
    "open": "set",
    "close": "set",
    "content": "set"
  };

  var methods_common = {
  }

  $.fn[PLUGIN_NAME] = function(arg){
    if(typeof arg == 'string'){
      var a = Array.prototype.slice.call(arguments, 1);
      var type = methods_types[arg];

      switch(type){
        case "set":
          return this.each(function(){
            $(this).data(PLUGIN_NAME).methods[arg].apply(this, a);
          });

        case "get":
          if(this.length > 1){
            console.warn("Вызов метода '" + arg + "' для нескольких DOM-элементов!"
              + " Отработает метод только для первого элемента из набора.");
          }
          return this.eq(0).data(PLUGIN_NAME).methods[arg].apply(this, a);

        default:
          console.warn("Вызов несуществующего метода '" + arg + "'!");
          return this;
      }
    }
    if(!(typeof arg == 'object' || typeof arg == 'undefined')) return;

    return this.each(function(){
      var options = $.extend({}, defaults, arg);
      var $elem = $(this);
      /**/

      var $content = $elem.find(".popup-modal-content");

      $elem
        .on("click.popup_modal", function(e){
          if($(e.target).is($elem)){
            close();
          }
        });

      $(document)
        .on("keydown.popup_modal", function(e){
          if(e.which == 27){
            close();
          }
        });

      var open = function(){
        $elem.stop().fadeIn(options.duration, function(){
          $(this).trigger("open");
        });
      };

      var close = function(){
        $elem.stop().fadeOut(options.duration, function(){
          $(this).trigger("close");
        });
      };

      // data
      var methods = {
        "open": function(){
          open();
        },

        "close": function(){
          close();
        },

        "content": function(content){
          $content.html(content);
        }
      };

      $elem.data(PLUGIN_NAME, {
        "methods": $.extend({}, methods, methods_common)
      });
    });
  }
})(jQuery, window, document);
