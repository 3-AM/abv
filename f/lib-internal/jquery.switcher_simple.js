/*
 *  --
 *  solodoff@ya.ru
 *  upd 14.10.2013
 *  --
 *  Выключатель
 *
 *
 *  Пример HTML структуры, на которую вешается плагин:
 *    <div class="switcher_simple checked">
 *      <div class="control"><div class="handle"></div></div>
 *      <span class="label">Уведомлять о безобразии по почте</span>
 *    </div>
 *
 *
 *  Зависимости:
 *    jquery.ui.draggable.js
 *
 *  Инициализация:
 *    $elem.switcher_simple({options});
 *
 *  События:
 *    $elem.on("switch.switcher_simple", function(e, checked){...});
 *
 *  Методы:
 *
 */
;(function ($, window, document, undefined){
  var PLUGIN_NAME = "switcher_simple";

  var defaults = {
    "duration": 200,
    "boundPos": 10,
    "checkedPos": 18,
    "disabled": false
  };

  var methods_types = {
    "disabled": "set"
  };

  var methods_common = {
  }


  $.fn[PLUGIN_NAME] = function(arg){
    if(typeof arg == 'string'){
      var a = Array.prototype.slice.call(arguments, 1);
      var type = methods_types[arg];

      switch(type){
        case "set":
          return this.each(function(){
            $(this).data(PLUGIN_NAME).methods[arg].apply(this, a);
          });

        case "get":
          if(this.length > 1){
            console.warn("Вызов метода '" + arg + "' для нескольких DOM-элементов!"
              + " Отработает метод только для первого элемента из набора.");
          }
          return this.eq(0).data(PLUGIN_NAME).methods[arg].apply(this, a);

        default:
          console.warn("Вызов несуществующего метода '" + arg + "'!");
          return this;
      }
    }
    if(!(typeof arg == 'object' || typeof arg == 'undefined')) return;

    return this.each(function(){
      var options = $.extend({}, defaults, arg);
      var $elem = $(this);
      /**/

      var $control = $elem.find(".control");
      var $handle = $control.find(".handle");
      var $label = $elem.find(".label");

      var toggle = function(){
        if($elem.is(".disabled")) return;
        if($elem.hasClass("checked")){
          switchOff();
        }else{
          switchOn();
        }
      };

      var switchOff = function(isFast){
        $handle.stop().animate({
          "left": 0 + "px"
        },{
          "duration": isFast ? 0 : options.duration,
          "complete": function(){
            $elem
              .removeClass("checked")
              .trigger("switch", [false]);
          },
          "progress": refresh_bg_pos
        });
      };

      var switchOn = function(isFast){
        $handle.stop().animate({
          "left": options.checkedPos + "px"
        },{
          "duration": isFast ? 0 : options.duration,
          "complete": function(){
            $elem
              .addClass("checked")
              .trigger("switch", [true]);
          },
          "progress": refresh_bg_pos
        });
      };

      var disable = function(){
        $elem.addClass("disabled");
        $handle.draggable("option", "disabled", true);
      };

      var enable = function(){
        $elem.removeClass("disabled");
        $handle.draggable("option", "disabled", false);
      };

      var refresh_bg_pos = function(){
        var left = parseInt($handle.css("left"), 10);
        var bg_pos = left + 9;
        $control.css("background-position", bg_pos+"px 0");
      };

      $handle
        .draggable({
          "axis": "x",
          "containment": "parent",
          "drag": refresh_bg_pos
        })
        .on('dragstop.switcher_simple', function(e, ui){
          if(ui.position.left  < options.boundPos){
            switchOff();
          }else{
            switchOn();
          }
        });

      $control.add($label).on("click.switcher_simple", toggle);

      // init
      if($elem.hasClass("checked")){
        switchOn(true);
      }else{
        switchOff(true);
      }

      $elem.css({
        "visibility": "visible"
      });


      // data
      var methods = {
        "disabled": function(value){
          if(value){
            disable();
          }else{
            enable();
          }
        }
      };

      $elem.data(PLUGIN_NAME, {
        "methods": $.extend({}, methods, methods_common)
      });
    });
  }
})(jQuery, window, document);
