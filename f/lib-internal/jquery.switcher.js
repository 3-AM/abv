/*
 *  --
 *  solodoff@ya.ru
 *  upd 21.11.2013
 *  --
 *  Переключатель
 *
 *
 *  Пример HTML структуры, на которую вешается плагин:
 *    <div class="switcher" data-switcher-items="["250  г","500 г","1 кг","3 кг"]">
 *      <div class="inner">
 *        <div class="handle"></div>
 *        <div class="sizer"></div>
 *      </div>
 *    </div>
 *
 *
 *  Зависимости:
 *    jquery.ui.draggable.js
 *    jquery.touch.js by Solodoff
 *
 *  Инициализация:
 *    $elem.switcher({options});
 *
 *  События:
 *    $elem.on('switch.switcher', function(e, item){...});
 *
 *  Методы:
 *
 */
;(function ($, window, document, undefined){
  var PLUGIN_NAME = 'switcher';

  var defaults = {
    'points_interval': 39, // px
    'point_size': 3 // px нужно для вычисления ширины
  };

  var methods_types = {
  };

  var methods_common = {
  }


  $.fn[PLUGIN_NAME] = function(arg){
    if(typeof arg == 'string'){
      var a = Array.prototype.slice.call(arguments, 1);
      var type = methods_types[arg];

      switch(type){
        case 'set':
          return this.each(function(){
            $(this).data(PLUGIN_NAME).methods[arg].apply(this, a);
          });

        case 'get':
          if(this.length > 1){
            console.warn("Вызов метода '" + arg + "' для нескольких DOM-элементов!"
              + " Отработает метод только для первого элемента из набора.");
          }
          return this.eq(0).data(PLUGIN_NAME).methods[arg].apply(this, a);

        default:
          console.warn("Вызов несуществующего метода '" + arg + "'!");
          return this;
      }
    }
    if(!(typeof arg == 'object' || typeof arg == 'undefined')) return;

    return this.each(function(){
      var options = $.extend({}, defaults, arg);
      var $elem = $(this);
      /**/

      var $handle = $elem.find('.handle');
      var $sizer = $elem.find('.sizer');


      var getMarginLeft = function(){
        var $testSwitcher = $(
          '<div class="switcher"><div class="inner"><div class="handle"></div><div class="sizer"></div></div></div>'
        ).css({'visiblity': 'hidden'}).appendTo('body');

        var res = $testSwitcher.find('.handle').html(items[0]).outerWidth();
        $testSwitcher.remove();

        return Math.round(res/2);
      };


      // data-switcher-items
      var data_items = JSON.parse($elem.attr('data-switcher-items'));
      $elem.removeAttr('data-switcher-items');

      /**
       * Хеш, координаты ползунка и выбранные значения
       * @type {object}
       */
      var items = {};
      for(var i=0; i<data_items.length; i++){
        var value = data_items[i];
        var coord = i * options.points_interval;
        items[coord] = value;
      }

      var width = options.points_interval * (data_items.length - 1) + options.point_size;

      $sizer.css('width', width + 'px');

      var pos = 0;
      $handle
        .draggable({
          'axis': 'x',
          'containment': $sizer,
          'grid': [options.points_interval, 0],
          'cursor': 'url("/f/closedhand.cur"), pointer',

          'drag': function(e, ui){
            var pos_new = ui.position.left;
            if(pos_new != pos){
              pos = pos_new;
              setItem(Math.round(pos));
            }
          }
        })
        .on('mousedown', function(){
          $(this).addClass('active');
        })
        .on('mouseup mouseleave', function(){
          $(this).removeClass('active');
        });

      /**
       * Меняет текст на ползунке, атрибут data-value, триггерит событие
       * @param {number} pos
       * @return {undefined}
       */
      var setItem = function(pos){
        var value = items[pos];
        $handle.html(value);

        // центрирование
        var margin = -getMarginLeft();
        $handle.css('margin-left', margin+'px');

        $elem
          .attr('data-value', value)
          .trigger('switch.switcher', [value]);
      };

      // helpers
      var $helper_tpl = $('<div/>').addClass('helper').append([
        $('<div/>').addClass('inner'),
        $('<div/>').addClass('icon')
      ]);
      var helpers = [];
      var position;

      for(var i=0; i<data_items.length; i++){
        position = options.points_interval * i;
        helpers.push(
          $helper_tpl
            .clone(false)
            .css('left', position + 'px')
            .data('position', position)
        );
      }

      $elem
        .on('click.switcher', '.helper .inner', function(){
          var position = $(this).closest('.helper').data('position');
          $handle.css('left', position + 'px');
          setItem(position);
        })
        .append(helpers);


      // init
      setItem(0);
      $handle.touch();
      if(data_items.length == 1){
        $elem.addClass('singleItem');
        $handle.draggable('disable');
      }

      //свитчер выравнивается по левой части плашки в левом положении:
      $elem.css('margin-left', 2 + getMarginLeft() + 'px');

      $elem.css({
        'width': width + 'px',
        'visibility': 'visible'
      });


      // data
      var methods = {
      };

      $elem.data(PLUGIN_NAME, {
        'methods': $.extend({}, methods, methods_common)
      });
    });
  }
})(jQuery, window, document);
