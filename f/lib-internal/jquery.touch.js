/*
 *  --
 *  solodoff@ya.ru
 *  upd 20.11.2013
 *  --
 *  Превращает touch-события на элементе в mouse-события.
 *
 *  Оригинальный скрипт взят отсюда http://www.jquery4u.com/mobile/jquery-add-dragtouch-support-ipad/
 *
 *  Инициализация:
 *    $(elem).touch();
 *
 */
;(function ($, window, document, undefined){
  var PLUGIN_NAME = 'touch';

  var defaults = {
  };

  var methods_types = {
  };

  var methods_common = {
  }

  var handleTouch = function(event){
    var touches = event.changedTouches;
    var first = touches[0];
    var type = '';

    switch(event.type){
      case 'touchstart':
        type = 'mousedown';
        break;

      case 'touchmove':
        type = 'mousemove';
        event.preventDefault();
        break;

      case 'touchend':
        type = 'mouseup';
        break;

      default:
        return;
    }

    var simulatedEvent = document.createEvent('MouseEvent');
    simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
    first.target.dispatchEvent(simulatedEvent);
  };



  $.fn[PLUGIN_NAME] = function(arg){
    if(typeof arg == 'string'){
      var a = Array.prototype.slice.call(arguments, 1);
      var type = methods_types[arg];

      switch(type){
        case "set":
          return this.each(function(){
            $(this).data(PLUGIN_NAME).methods[arg].apply(this, a);
          });

        case "get":
          if(this.length > 1){
            console.warn("Вызов метода '" + arg + "' для нескольких DOM-элементов!"
              + " Отработает метод только для первого элемента из набора.");
          }
          return this.eq(0).data(PLUGIN_NAME).methods[arg].apply(this, a);

        default:
          console.warn("Вызов несуществующего метода '" + arg + "'!");
          return this;
      }
    }
    if(!(typeof arg == 'object' || typeof arg == 'undefined')) return;

    return this.each(function(){
      var options = $.extend({}, defaults, arg);
      var $elem = $(this);
      /**/

      $elem.on('touchstart touchmove touchend touchcancel',function(){
        //we pass the original event object because the jQuery event
        //object is normalized to w3c specs and does not provide the TouchList
        handleTouch(event);
      });


      // data
      var methods = {
      };

      $elem.data(PLUGIN_NAME, {
        "methods": $.extend({}, methods, methods_common)
        /* ... */
      });
    });
  }
})(jQuery, window, document);
