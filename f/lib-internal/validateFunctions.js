Validator.prototype.DEFAULTS.validateFunctions = {
  'phone': function(field){
    var pattern = /^\+7 \d\d\d \d\d\d \d\d \d\d$/gi;
    return pattern.test(field.value);
  },

  'email': function(field){
    var pattern = /^\S+@\S+$/gi;
    return pattern.test(field.value);
  },

  'noempty': function(field){
    return field.value !== '';
  },

  'number': function(field){
    var pattern = /^\d+$/gi;
    return pattern.test(field.value);
  },

  'captcha': function(field){
    return !!$(field).find('.itemset-item.selected').length;
  }
};