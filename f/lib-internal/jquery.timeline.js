/*
 *  --
 *  solodoff@ya.ru
 *  upd 24.11.2013
 *  --
 *  timeline
 *
 *
 */
;(function ($, window, document, undefined){
  var PLUGIN_NAME = 'timeline';

  var defaults = {
    'basketWidth': 15,
    'intervalWidth': 37,
    'intervals': [
      ['11:00', '13:00'],
      ['13:00', '15:00'],
      ['15:00', '17:00'],
      ['17:00', '20:00'],
      ['20:00', '23:00']
    ]
  };

  var methods_types = {
  };

  var methods_common = {
  }



  $.fn[PLUGIN_NAME] = function(arg){
    if(typeof arg == 'string'){
      var a = Array.prototype.slice.call(arguments, 1);
      var type = methods_types[arg];

      switch(type){
        case 'set':
          return this.each(function(){
            $(this).data(PLUGIN_NAME).methods[arg].apply(this, a);
          });

        case 'get':
          if(this.length > 1){
            console.warn("Вызов метода '" + arg + "' для нескольких DOM-элементов!"
              + " Отработает метод только для первого элемента из набора.");
          }
          return this.eq(0).data(PLUGIN_NAME).methods[arg].apply(this, a);

        default:
          console.warn("Вызов несуществующего метода '" + arg + "'!");
          return this;
      }
    }
    if(!(typeof arg == 'object' || typeof arg == 'undefined')) return;

    return this.each(function(){
      var options = $.extend({}, defaults, arg);
      var $elem = $(this);
      /**/

      var $currentTime = $elem.find('.timeline-currentTime');
      var amount = $elem.find('li').length - 1;


      var proportion = function (a1, a2, a, b1, b2) {
        var p = (a - a1) / (a2 - a1);
        if (p < 0) p = 0;
        if (p > 1) p = 1;
        return b1 + (b2 - b1) * p;
      };

      /**
       * @param {Array|String} arg '23:01' или [23, 1]
       * @returns {Number}
       */
      var getMinutes = function(arg){
        var h, m;

        if(typeof(arg) == 'string'){
          h = parseInt(arg.split(':')[0], 10);
          m = parseInt(arg.split(':')[1], 10);
        }else{
          h = arg[0];
          m = arg[1];
        }

        return h*60 + m;
      };


      /**
       * @param {Array} intervals
       * @returns {Object} {
       *  'index': индекс интервала,
       *  't1': начальное время в мин,
       *  't2': конечное время в мин,
       *  't': текущее время в мин
       * }
       */
      var findInterval = function(intervals){
        var res = [-1, 0, 0];

        var now = new Date();
        var h = now.getHours();
        var m = now.getMinutes();

        var t = getMinutes([h, m]);

        for(var i=0; i<intervals.length; i++){
          var interval = intervals[i];
          var t1 = getMinutes(interval[0]);
          var t2 = getMinutes(interval[1]);

          if(t1 < t2){
            if(t >= t1 && t < t2){
              res = [i, t1, t2, t];
              break;
            }
          }else{
            if(t >= t1 || t < t2){
              res = [i, t1, t2, t];
              break;
            }
          }
        }
        
        return {
          'index': res[0],
          't1' : res[1],
          't2' : res[2],
          't' : res[3]
        };
      };


      var getProgress = function(t1, t2, t){
        var t0 = 24*60;
        var delta;
        if(t1>t2){
          delta = t0 - t1;
          t1 = 0;
          t2 += delta;
          t += delta;
        }

        return proportion(t1, t2, t, 0, 100);
      };


      // находим интервал и вычисляем позицию тележки
      if($currentTime.length){
        var res = findInterval(options.intervals);

        if(res.index != -1){
          var progress = getProgress(res.t1, res.t2, res.t);
          var pos = options.intervalWidth * res.index + (options.intervalWidth - options.basketWidth) * progress / 100;
          pos = Math.round(pos);
          $currentTime.css({
            'visibility': 'visible',
//            'background-position': pos + 'px 0px'
            'left': pos + 'px'
          });
        }else{
          $currentTime.hide();
        }
      }


      // data
      var methods = {
      };

      $elem.data(PLUGIN_NAME, {
        'methods': $.extend({}, methods, methods_common)
        /* ... */
      });
    });
  }
})(jQuery, window, document);
