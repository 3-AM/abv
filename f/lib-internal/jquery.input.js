/*
 *  --
 *  solodoff@ya.ru
 *  upd 20.10.2013
 *  --
 *
 *  Функционал инпута
 *
 *
 *  Зависимости:
 *
 *  Инициализация:
 *    <div class="input" data-placeholder="логин">
 *      <div class="placeholder">логин</div>
 *      <input type="text"/>
 *    </div>
 *
 *    $('.input').input();
 *
 *  События:
 *
 *  Методы:
 *
 *  Особенности:
 *    Если значение поля меняется скриптом, то для корректной работы плейсхолдера
 *    необходимо вызвать placeholderInit. Другой путь - менять value через метод
 *    .input("val", "...")
 *
 */
;(function ($, window, document, undefined){
  var PLUGIN_NAME = "input";

  var PLACEHOLDER_MODE_BLUR = 1;
  var PLACEHOLDER_MODE_EMPTY = 2;

  var defaults = {
  };

  var methods_types = {
    "placeholderOn": "set",
    "placeholderOff": "set",
    "placeholderInit": "set",
    "val": "set"
  };

  var methods_common = {
  }

  $.fn[PLUGIN_NAME] = function(arg){
    if(typeof(arg) == 'string'){
      var a = Array.prototype.slice.call(arguments, 1);
      var type = methods_types[arg];

      switch(type){
        case "set":
          return this.each(function(){
            $(this).data(PLUGIN_NAME).methods[arg].apply(this, a);
          });

        case "get":
          if(this.length > 1){
            console.warn("Вызов метода '" + arg + "' для нескольких DOM-элементов!"
              + " Отработает метод только для первого элемента из набора.");
          }
          return this.eq(0).data(PLUGIN_NAME).methods[arg].apply(this, a);

        default:
          console.warn("Вызов несуществующего метода '" + arg + "'!");
          return this;
      }
    }
    if(!(typeof(arg) == 'object' || typeof(arg) == 'undefined')) return;

    return this.each(function(){
      var options = $.extend({}, defaults, arg);
      var $elem = $(this);

      if(typeof($elem.data(PLUGIN_NAME)) == 'undefined'){
        $elem.data(PLUGIN_NAME, {});
      }else{
        return;
      }
      /**/

      var $input = $elem.find("input");
      var $placeholder = $elem.find(".placeholder");
      var $label = $elem.find("label");

      // focus
      $input
        .on("focus.input", function(){
          $elem.addClass("focus");
        })
        .on("blur.input", function(){
          $elem.removeClass("focus");
        });

      // label
      $label.on("click.input", function(){
        $input.focus();
      });

      //popup
      var popupContent = $input.attr('data-popup-content');
      if(typeof(popupContent) != 'undefined'){
        $input.popup({
          'extraClass': 'forInput',
          'isHidden': true
        });
      }

      // placeholder
      if($elem.hasClass("hasPlaceholder")){
        var placeholder_mode = $elem.hasClass("placeholderModeBlur") ? PLACEHOLDER_MODE_BLUR : PLACEHOLDER_MODE_EMPTY;

        // Режим работы плейсхолдера
        if(placeholder_mode == PLACEHOLDER_MODE_BLUR){
          $input
            .on("focus.input", function(){
              methods.placeholderOff();
            })
            .on("blur.input", function(){
              if($input.val() == ""){
                methods.placeholderOn();
              }
            });
        }else{
          $input.on("input.input keyup.input", function(){
            if($input.val() == ""){
              methods.placeholderOn();
            }else{
              methods.placeholderOff();
            }
          });
        }
      }

      var placeholderInit = function(){
        //methods.placeholderOff();
        $placeholder.hide();

        if(placeholder_mode == PLACEHOLDER_MODE_BLUR){
          if(!$input.is(":focus")){
            $input.trigger("blur");
          }
        }else{
          $input.trigger("input");
        }
      };

      // data
      var methods = {
        "placeholderOn": function(){
          $placeholder.show();
        },

        "placeholderOff": function(){
          $placeholder.hide();
        },

        "placeholderInit": function(){
          placeholderInit();
        },

        "val": function(value){
          $input.val(value);
          placeholderInit();
        }
      };

      $elem.data(PLUGIN_NAME, {
        "methods": $.extend({}, methods, methods_common)
      });

      // init
      placeholderInit();
    });
  }
})(jQuery, window, document);
