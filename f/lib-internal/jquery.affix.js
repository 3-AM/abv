/*!
 * jQuery Affix micro plugin
 * Author: Prontiol <prontiol@gmail.com>
 * Licensed under the MIT license
 */
;(function ($, window, document, undefined) {
    var defaults = {
    };
    $.fn.extend({
        affix: function(overrides) {
            var options = $.extend({}, defaults, overrides);
            return this.each(function() {
                var $self = $(this);
                var $wrapper = $self.parent();
                var $window = $(window);
                var $b = (options.bottomBound ? $(options.bottomBound) : undefined);
                $window.on('scroll.affix resize.affix', function() {
                    var skipAffix = ($b && ($b.outerHeight() < $self.outerHeight()));
                    if (!skipAffix && ($window.scrollTop() > $wrapper.offset().top)) {
                        var y = 0;
                        if ($b) {
                          y = Math.min(0, $b.position().top+$b.outerHeight()-$(window).scrollTop()-$self.outerHeight());
                        }
                        $self.addClass('affix').css({
                            position: 'fixed',
                            top: y,
                            left: $wrapper.offset().left-$window.scrollLeft(),
                            width: $self.outerWidth()
                        });
                    } else {
                        $self.removeClass('affix').css({
                            position:'relative',
                            top: 0,
                            left: 0,
                            width: ''
                        });
                    }
                });
                $(window).trigger('scroll');
            });
        }
    });
})(jQuery, window, document);
