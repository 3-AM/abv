/*!
 * jQuery Itemset micro plugin
 * Author: Prontiol <prontiol@gmail.com>
 * Licensed under the MIT license
 *
 * Events:
 * $(document).on('changeState.itemset', function(e, $itemsetNode, [status array]){...});
 *
 */
;(function ($, window, document, undefined) {
  var defaults = {
    multiselect: false,
    deselect: false
  };
  $.fn.extend({
    itemset: function(overrides) {
      return this.each(function() {
        var $itemset = $(this);
        var dataOptions = $itemset.data('itemset-options');
        var options = $.extend({}, defaults, overrides, dataOptions);
        options.multiselect = options.multiselect || ($itemset.hasClass('multiselect'));
        options.deselect = options.deselect || ($itemset.hasClass('deselect'));
        $itemset.find('.itemset-item').each(function() {
          var $itemsetItem = $(this);
          $itemsetItem
          .on('selectstart', function() {
            return false;
          })
          .on('click.itemset mouseup.itemset', function() {
            var status = [];
            if (!$itemsetItem.hasClass('disabled')) {
              if (options.multiselect || options.deselect) {
                $itemsetItem.toggleClass('selected');
                if (options.deselect) {
                  $itemsetItem.siblings('.itemset-item').removeClass('selected');
                }
                //
                $itemset.find('.itemset-item').each(function(){ status.push($(this).hasClass('selected')); });
                $itemsetItem.trigger('changeitem');
              } else {
                if(!$(this).hasClass('selected')){
                  $itemset.find('.itemset-item').removeClass('selected');
                  $itemsetItem.addClass('selected');
                  //
                  $itemset.find('.itemset-item').each(function(){ status.push($(this).hasClass('selected')); });
                  $itemsetItem.trigger('changeitem');
                }
              }

            }
          })
          .bind('clickitemset', options.click);
        });
      });
    }
  });
})(jQuery, window, document);
