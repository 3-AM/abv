/*
 * --
 * solodoff@ya.ru
 * upd 30.10.2013
 * --
 *
 */

ko.bindingHandlers.stopBindings = {
  init: function() {
    return {
      controlsDescendantBindings: true
    };
  }
};
ko.bindingHandlers.afterRenderAll = {
  update: function(element, valueAccessor, allBindingsAccessor) {
    // add dependency on all other bindings
    ko.toJS(allBindingsAccessor());
    setTimeout(function() {
      var value = valueAccessor();
      if (typeof value != 'function' || ko.isObservable(value))
        throw new Error('run must be used with a function');
      value(element);
    });
  }
};
