;(function($, window, document, undefined) {
  'use strict';

  $(function() {
    // catalog
    var $basket = $('.basket-main');

    // filters
    function clearFilters() {
      $basket.find('.basket-filter').removeClass('with-icon_down with-icon_up').addClass('with-icon_sort-placeholder');
    }
    $basket.find('.basket-filter .link-text').on('click', function() {
      var $filter = $(this).parent();
      if ($filter.is('.with-icon_sort-placeholder')) {
        clearFilters();
        $filter.removeClass('with-icon_sort-placeholder').addClass('with-icon_down');
      } else if ($filter.is('.with-icon_down')) {
        clearFilters();
        $filter.removeClass('with-icon_sort-placeholder').addClass('with-icon_up');
      } else if ($filter.is('.with-icon_up')) {
        clearFilters();
        $filter.removeClass('with-icon_sort-placeholder').addClass('with-icon_down');
      }
      if ($filter.is('.with-icon_sort-placeholder')) {
        window.abv.basket.sortProducts('timestamp', true);
      } else {
        window.abv.basket.sortProducts($filter.data('sort'), $filter.is('.with-icon_down'));
      }
    });

    // points
    $('.basket-bill-points-text').on('click', function() {
      $('.basket-bill-points-form').slideToggle('fast', function() {
        $(this).find('input').focus();
      });
    });

    // promo
    $('.basket-bill-promo').on('click', function() {
      $('.basket-bill-promo-form').slideToggle('fast', function() {
        $(this).find('input').focus();
      });
    });

    $('.basket-bill-promo-button').on('click', function() {
      $('.basket-bill-promo-input').slideUp('fast');
    });

    // relay
    $('.basket-relay-button').on('click', function() {
      $('.basket-relay-link').slideToggle('fast').find('input').select().focus();
    });

    // basket
    $('.basket-button').on('click', function(event) {
      $('.basket-side-content').css('top', 0);
      $('.popups-fader').show();
      $('.popups').show().find('.basket-popup').show();
      $('body, html').animate({scrollTop: 0}, 0);
      $('body').css('overflow', 'hidden');
      event.stopPropagation();
    });

    $('.basket-side-content').affix({bottomBound: $basket});

    $('.basket-main, .basket-side').on('click', function(event) {
      event.stopPropagation();
    });



  });

})(jQuery, window, document);