var valid1;
var valid2;

;(function($, window, document, undefined) {
  'use strict';

  $(function() {
	orderScripts();
  });
  
})(jQuery, window, document);

BX.addCustomEvent('onAjaxSuccess', function(){

	$('select').chosen({disable_search: true});
    $('.fold').foldable();
    $('.itemset:not(.notApplyPlugin)').itemset();

	valid1.refresh(true);
	valid2.refresh(true);
	
    $('.mask-phone input').val('+7').mask(abv.mask.phone, {placeholder: ''})
      .on('click', function(){
        abv.caretFix(this);
      });
    $('.mask-number-natural input').mask(abv.mask.number_natural, {placeholder: ''});
	
	orderScripts();
});

function orderScripts() {

    // init map
    ymaps.ready(function() {
      var placemark = new ymaps.Placemark([55.8, 37.6], {
        hintContent: 'Центральный офис',
        balloonContentHeader: '<h5 class="balloonTitle">Центральный офис</h5>',
        balloonContentBody:
          '<b>Адрес:</b> Анненский пр., д. 1, стр. 20, Москва<br/>'+
            '<b>Телефон:</b> +7 499 707-07-33<br/>'+
            '<b>Электронная почта:</b> <a href="info@abv-store.ru">info@abv-store.ru</a><br/>'
      }, {
        iconImageHref: '/f/placemark.png',
        iconImageSize: [46, 47],
        iconImageOffset: [-15, -43]
      });

      var map = new ymaps.Map('map', {
        center: placemark.geometry.getCoordinates(),
        zoom: 10
      });
      map.controls.add('mapTools');
      map.controls.add('zoomControl');
      map.controls.add('scaleLine');
      map.geoObjects.add(placemark);

      //window.abv.map = map;
    });
	
	// Валидация форм
	var applyValidation = function($form){
	  var $submit = $form.find('.button-green');
	  var selectorFields = '.required input';
	  var options = {
		'selector': selectorFields,

		'fields': {
		  'ORDER_PROP_8': { 'validatorFuncName': 'noempty' },
		  'ORDER_PROP_9': { 'validatorFuncName': 'phone' },
		  'ORDER_PROP_81': { 'validatorFuncName': 'noempty' },
		  'ORDER_PROP_91': { 'validatorFuncName': 'phone' },
		  'ORDER_PROP_10': { 'validatorFuncName': 'email' },
		  'ORDER_PROP_2': { 'validatorFuncName': 'noempty' },
		  'ORDER_PROP_3': { 'validatorFuncName': 'noempty' }
		}
	  };

	  validator = new Validator($form, options);

	  $form.on('changeState', function(e, isChanged, isValid, remainingFields){
		if(isChanged){
		  if(isValid){
			$submit.removeAttr('disabled');
		  }else{
			$submit.attr('disabled', 'disabled');
		  }
		}
	  });

	  $form.find(selectorFields).on('changeState', function(e, isValid){
		if(!$(this).data('popup')) return;

		if(isValid){
		  $(this).popup('hide');
		}else{
		  $(this).popup('show');
		}
	  });
	  
	  return validator;
	};

	valid1 = applyValidation($('.form-delivery'));
	valid2 = applyValidation($('.form-self-delivery'));

    $('#ORDER_FORM').on('click', '.tab-header', function() {
		$('.tab-content').find('input').attr('disabled', 'disabled');
		$('.tabs-contents').find('.tab-content').eq($(this).index()).show().find('input').removeAttr('disabled');
		$('.tabs-contents').find('.tab-content').eq($(this).index()).show().siblings().hide();
		submitForm();
    });
	$('.tab-content:hidden').find('input').attr('disabled', 'disabled');
	
	
	//интервалы
	$('#interval_day .itemset').find('.itemset-item').first().addClass('selected');
	$('#interval_time').find('.itemset').first().show().siblings().hide();
	$('#interval_day .itemset-item').click(function(){
		$('#'+$(this).attr('rel')).show().siblings().hide();
	});
	$('#interval_day .itemset-item, #interval_time .itemset-item').click(function(){
		var day = '';
		var time = '';
		
		day = $('#interval_day .itemset-item.selected').data('date')
		if ($('#interval_time .itemset-item.selected:visible').find('span')) {
			time = $('#interval_time .itemset-item.selected:visible').find('span').html();
		}
		value = day + ' ' + time;
		$('#ORDER_PROP_7').val(value);
	});
	
}