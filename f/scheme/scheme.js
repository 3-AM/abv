;(function($, window, document, undefined) {
  'use strict';

  $(function() {
    $('.scheme_legend').draggable();
    $('.scheme_legend_close').on('click', function() {
      $(this).parent().fadeOut('fast');
    });
  });

})(jQuery, window, document);
