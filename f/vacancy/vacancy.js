$(function() {
  'use strict';

  var $form = $(".form");
  var $submit = $form.find('.form-submit');

  (function(){
    var selectorFields = 'input[name], textarea[name]';
    var options = {
      'selector': selectorFields,

      'fields': {
        'name': { 'validatorFuncName': 'noempty' },
        'phone': { 'validatorFuncName': 'phone' },
        'message': { 'validatorFuncName': 'noempty' }
      }
    };
    var validator = new Validator($form, options);

    $form.on('changeState', function(e, isChanged, isValid, remainingFields){
      if(isChanged){
        if(isValid){
          $submit.removeAttr('disabled');
        }else{
          $submit.attr('disabled', 'disabled');
        }
      }
    });

    $(selectorFields).on('changeState', function(e, isValid){
      if(!$(this).data('popup')) return;

      if(isValid){
        $(this).popup('hide');
      }else{
        $(this).popup('show');
      }
    });
  })();

})
