;(function($, window, document, undefined) {
  'use strict';

  $(function() {
    /* top */
    var $top = $('.top');
    var $topClose = $top.find('.top_close');
    $topClose.on('click', function() {
      $top.stop().animate({
        'margin-top': -$top.outerHeight()
      }, 'fast', function() {
        $top.hide();
      });
	  
	  var date = new Date;
	  date.setDate( date.getDate() + 1 );
	  document.cookie="act_show=1; path=/; expires="+date.toUTCString();

	  $('.ribbon').removeClass('has_top');
    });
  });

})(jQuery, window, document);